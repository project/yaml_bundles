node.test_bundle:
  label: Test bundle
  description: A description for the test bundle.
  langcode: en

  # Add generic node type settings.
  help: Help text for the test bundle.

  # Enable a custom path alias for the bundle. Requires the pathauto module to
  # be enabled.
  path: 'test-bundle/[node:title]'

  # Configure the simple_sitemap settings for the bundle. Requires the
  # simple_sitemap module to be enabled.
  sitemap:
    priority: 0.5

  # Configure the search API index boost for the bundle. Requires the
  # search_indexes to be configured and the search_api module to be enabled.
  boost: 1.5

  # Configure the fields for the bundle. For base fields, the field only needs
  # a label. For custom fields, the type needs to be specified. The type
  # configuration from the yaml_bundles.field_type plugins will be merged with
  # the field configuration to allow the definition to be relatively simple.
  # Generic defaults for a field type can be configured in the
  # yaml_bundles.field_type plugins.
  #
  # See yaml_bundles.yaml_bundles.field_type.yml for the list of supported
  # field types and their configuration properties.
  fields:

    field_extra_title:
      type: string
      label: Extra title
      required: true
      search: true
      search_boost: 10
      widget_settings:
        placeholder: This is a placeholder.
      form_displays:
        - default
        - short
      view_displays:
        - default
        - full
        - teaser

    field_summary:
      type: text_long
      label: Summary
      description: This is a description for the summary.
      required: false
      search: true
      search_boost: 5
      maxlength: 180
      maxlength_label: 'The content is limited to @limit characters, remaining: <strong>@remaining</strong>'
      form_displays:
        - default
        - short
      view_displays:
        - default
        - teaser

    field_body:
      type: text_long
      label: Text
      required: true
      search: true
      search_boost: 1
      form_displays:
        - default
      view_displays:
        - default
        - full

    field_media:
      type: entity_reference
      label: Media
      required: true
      storage_settings:
        target_type: media
      field_settings:
        handler: default:media
        handler_settings:
          target_bundles:
            - image
      widget_type: media_library_widget
      widget_settings:
        media_types:
        - image
      form_displays:
        - default
        - short
      view_displays:
        - default
        - full
        - teaser

    field_image:
      type: image
      label: Image
      required: true
      search: true
      form_displays:
        - default
        - short
      view_displays:
        - default
        - full
        - teaser

    field_category:
      type: list_string
      label: Category
      required: false
      search: true
      options:
        category_1: Category 1
        category_2: Category 2
      form_displays:
        - default
        - short
      view_displays:
        - default
        - full
        - teaser

    field_status:
      type: boolean
      label: Status
      required: false
      search: true
      formatter_label: above
      form_displays:
        - default
        - short
      view_displays:
        - default
        - full
        - teaser

    field_link:
      type: link
      label: Link
      required: true
      search: true
      cardinality: 2
      form_displays:
        - default
        - short
      view_displays:
        - default
        - full
        - teaser

    field_date:
      type: datetime
      label: Date
      required: false
      search: true
      search_boost: 1
      cardinality: 1
      field_default_value:
        -
          default_date_type: now
          default_date: now
      form_displays:
        - default
        - short
      view_displays:
        - default
        - full
        - teaser

    field_end_date:
      type: datetime
      label: End date
      required: false
      search: true
      cardinality: 1
      form_displays:
        - default
        - short
      view_displays:
        - default
        - full
        - teaser

    field_count:
      type: integer
      label: Count
      required: false
      min: 0
      max: 50
      default: 10
      search: true
      weight: 50
      form_displays:
        - default
        - short
      view_displays:
        - default
        - full
        - teaser

    field_blocks:
      type: entity_reference_revisions
      label: Blocks
      required: false
      cardinality: 5
      field_settings:
        handler_settings:
          target_bundles:
            block: block
      widget_settings:
        title: Block
        title_plural: Blocks
      form_displays:
        - default
        - short
      view_displays:
        - default
        - full
        - teaser

    field_third_party_settings:
      type: third_party_settings
      label: Third party settings
      description: This is a description for the third party settings.
      required: false
      maxlength: 180
      maxlength_label: 'The content is limited to @limit characters, remaining: <strong>@remaining</strong>'
      form_displays:
        - default
        - short
      view_displays:
        - default
        - full
        - teaser

    # The field_entity_text_removed field is removed from the bundle.
    field_entity_text_removed: null

  # Configure the groups for the bundle. The type configuration from the
  # yaml_bundles.group_type plugins will be merged with the group configuration
  # to allow the definition to be relatively simple. Generic defaults for a
  # group type can be configured in the yaml_bundles.group_type plugins.
  #
  # See yaml_bundles.yaml_bundles.group_type.yml for the list of supported
  # group types and their configuration properties.
  groups:

    group_main:
      fields:
        - field_extra_title

    group_content:
      type: tab
      label: Content
      parent: group_tabs
      fields:
        - group_description
        - field_date
        - field_end_date
        - group_meta
        - field_summary
        - field_body
        - field_link
        - field_blocks
      form_displays:
        - default

    group_description:
      type: markup
      label: Description
      parent: group_content
      description: This is a description for the content group.
      form_displays:
        - default
      view_displays:
        - default

    group_meta:
      type: markup
      label: Meta
      parent: group_content
      description: This is a description for the meta group.
      format: full_html
      form_displays:
        - default

    group_media:
      type: details
      label: Media
      parent: group_content
      fields:
        - field_media
        - field_image
      form_displays:
        - default

    group_settings:
      type: tab
      label: Settings
      parent: group_tabs
      fields:
        - field_category
        - field_status
        - field_count
      form_displays:
        - default

  # Allow the labels, descriptions and settings to be translated. The
  # translations can be configured for each enabled language ID.
  translations:
    nl:
      label: Test bundel
      description: Een beschrijving voor de test bundel.
      fields:
        langcode:
          label: Taal
        title:
          label: Administratieve titel
          description: Dit is een beschrijving voor de titel.
        field_extra_title:
          label: Extra titel
          widget_settings:
            placeholder: Dit is een placeholder.
        field_summary:
          label: Samenvatting
          description: Dit is een beschrijving voor de samenvatting.
          maxlength_label: 'De inhoud is beperkt tot @limit tekens, resterend: <strong>@remaining</strong>'
        field_body:
          label: Tekst
        field_media:
          label: Media
        field_image:
          label: Afbeelding
        field_category:
          label: Categorie
          options:
            category_1: Categorie 1
            category_2: Categorie 2
        field_status:
          label: Status
        field_date:
          label: Datum
        field_end_date:
          label: Einddatum
        field_count:
          label: Aantal
        field_blocks:
          label: Blokken
          widget_settings:
            title: Blok
            title_plural: Blokken
        field_third_party_settings:
          label: Derde partij instellingen
          description: Dit is een beschrijving voor de derde partij instellingen.
          maxlength_label: 'De inhoud is beperkt tot @limit tekens, resterend: <strong>@remaining</strong>'
      groups:
        group_content:
          label: Inhoud
        group_description:
          label: Beschrijving
          description: Dit is een beschrijving voor de inhoud groep.
        group_meta:
          label: Meta
          description: Dit is een beschrijving voor de meta groep.
        group_media:
          label: Media
        group_settings:
          label: Instellingen
