<?php

namespace Drupal\Tests\yaml_bundles\Kernel;

use Drupal\node\NodeTypeInterface;

/**
 * Tests the content moderation support of yaml_bundles.bundle plugins.
 *
 * @group yaml_bundles
 */
class ContentModerationTest extends YamlBundlesKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'content_moderation',
    'datetime',
    'entity_reference_revisions',
    'field',
    'file',
    'filter',
    'image',
    'language',
    'link',
    'media',
    'media_library',
    'node',
    'options',
    'paragraphs',
    'system',
    'text',
    'user',
    'views',
    'workflows',
    'yaml_bundles',
    'yaml_bundles_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create an editorial workflow.
    $this->installConfig(['content_moderation']);
    $this->container->get('entity_type.manager')->getStorage('workflow')->create([
      'type' => 'content_moderation',
      'id' => 'editorial',
      'label' => 'Editorial',
      'type_settings' => [
        'states' => [
          'archived' => [
            'label' => 'Archived',
            'weight' => 5,
            'published' => FALSE,
            'default_revision' => TRUE,
          ],
          'draft' => [
            'label' => 'Draft',
            'published' => FALSE,
            'default_revision' => FALSE,
            'weight' => -5,
          ],
          'published' => [
            'label' => 'Published',
            'published' => TRUE,
            'default_revision' => TRUE,
            'weight' => 0,
          ],
        ],
        'transitions' => [
          'archive' => [
            'label' => 'Archive',
            'from' => ['published'],
            'to' => 'archived',
            'weight' => 2,
          ],
          'archived_draft' => [
            'label' => 'Restore to Draft',
            'from' => ['archived'],
            'to' => 'draft',
            'weight' => 3,
          ],
          'archived_published' => [
            'label' => 'Restore',
            'from' => ['archived'],
            'to' => 'published',
            'weight' => 4,
          ],
          'create_new_draft' => [
            'label' => 'Create New Draft',
            'to' => 'draft',
            'weight' => 0,
            'from' => [
              'draft',
              'published',
            ],
          ],
          'publish' => [
            'label' => 'Publish',
            'to' => 'published',
            'weight' => 1,
            'from' => [
              'draft',
              'published',
            ],
          ],
        ],
      ],
    ])->save();
  }

  /**
   * Tests the creation of a bundle using yaml_bundles.bundle plugins.
   */
  public function testContentModerationSettings(): void {
    $entity_type_manager = $this->container->get('entity_type.manager');
    $bundle_creator = $this->container->get('yaml_bundles.bundle_creator');

    $node_type_storage = $entity_type_manager->getStorage('node_type');

    // Create the bundles from the yaml_bundles.bundle plugins.
    $bundle_creator->createBundles();

    // Assert the node type is created.
    $node_type = $node_type_storage->load('test_bundle');
    static::assertInstanceOf(NodeTypeInterface::class, $node_type);

    // Assert the bundle is added to the editorial workflow.
    $workflow = $entity_type_manager->getStorage('workflow')->load('editorial');
    static::assertSame(['test_bundle'], $workflow->getTypePlugin()->getBundlesForEntityType('node'));
  }

}
