<?php

namespace Drupal\Tests\yaml_bundles\Kernel;

use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\node\NodeTypeInterface;

/**
 * Tests the third party settings of yaml_bundles.bundle plugins.
 *
 * @group yaml_bundles
 */
class ThirdPartySettingsTest extends YamlBundlesKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'allowed_formats',
    'content_translation',
    'datetime',
    'entity_reference_revisions',
    'field',
    'file',
    'filter',
    'image',
    'language',
    'link',
    'maxlength',
    'media',
    'media_library',
    'node',
    'options',
    'paragraphs',
    'system',
    'text',
    'user',
    'views',
    'yaml_bundles',
    'yaml_bundles_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Enable the Dutch language.
    ConfigurableLanguage::createFromLangcode('nl')->save();
  }

  /**
   * Tests the creation of a bundle using yaml_bundles.bundle plugins.
   */
  public function testThirdPartySettings(): void {
    $entity_type_manager = $this->container->get('entity_type.manager');
    $config_factory = $this->container->get('config.factory');
    $language_manager = $this->container->get('language_manager');
    $bundle_creator = $this->container->get('yaml_bundles.bundle_creator');

    $node_type_storage = $entity_type_manager->getStorage('node_type');
    $field_storage_storage = $entity_type_manager->getStorage('field_storage_config');
    $field_instance_storage = $entity_type_manager->getStorage('field_config');
    $form_display_storage = $entity_type_manager->getStorage('entity_form_display');

    // Create the bundles from the yaml_bundles.bundle plugins.
    $bundle_creator->createBundles();

    // Assert the node type is created.
    $node_type = $node_type_storage->load('test_bundle');
    static::assertInstanceOf(NodeTypeInterface::class, $node_type);

    // The field storage third party settings to verify.
    $field_third_party_settings = [
      'field_third_party_settings' => [
        'storage_third_party_settings' => [
          'yaml_bundles_test' => [
            'storage_test' => 'test_value',
          ],
        ],
        'field_third_party_settings' => [
          'yaml_bundles_test' => [
            'field_test' => 'test_value',
          ],
        ],
      ],
    ];

    // Assert the third party settings are correctly configured for the field.
    foreach ($field_third_party_settings as $field_name => $settings) {
      $field_storage = $field_storage_storage->load('node.field_third_party_settings');
      $modules = $field_storage->getThirdPartyProviders();
      foreach ($modules as $module) {
        $settings = $field_storage->getThirdPartySettings($module);
        static::assertArrayHasKey($module, $field_third_party_settings[$field_name]['storage_third_party_settings'], sprintf('The third party settings of the %s custom field storage are not correctly configured.', $field_name));
        static::assertSame($field_third_party_settings[$field_name]['storage_third_party_settings'][$module], $settings, sprintf('The third party settings of the %s custom field storage are not correctly configured.', $field_name));
      }
      $field_instance = $field_instance_storage->load('node.test_bundle.field_third_party_settings');
      $modules = $field_instance->getThirdPartyProviders();
      foreach ($modules as $module) {
        $settings = $field_instance->getThirdPartySettings($module);
        static::assertArrayHasKey($module, $field_third_party_settings[$field_name]['field_third_party_settings'], sprintf('The third party settings of the %s custom field instance are not correctly configured.', $field_name));
        static::assertSame($field_third_party_settings[$field_name]['field_third_party_settings'][$module], $settings, sprintf('The third party settings of the %s custom field instance are not correctly configured.', $field_name));
      }
    }

    // The form display third party settings to verify.
    $form_third_party_settings = [
      'field_third_party_settings' => [
        'widget_third_party_settings' => [
          'allowed_formats' => [
            'hide_help' => '1',
            'hide_guidelines' => '1',
          ],
          'maxlength' => [
            'maxlength_js' => 180,
            'maxlength_js_label' => 'The content is limited to @limit characters, remaining: <strong>@remaining</strong>',
            'maxlength_js_enforce' => FALSE,
          ],
        ],
      ],
    ];

    // Assert the third party settings are correctly configured for the
    // maxlength module.
    foreach ($form_third_party_settings as $field_name => $settings) {
      $form_display_component = $form_display_storage->load('node.test_bundle.default')->getComponent($field_name);
      static::assertSame($form_display_component['third_party_settings'], $settings['widget_third_party_settings'], sprintf('The third party settings of the %s custom field are not correctly configured.', $field_name));
    }

    // Assert the third party settings are correctly translated.
    $language_manager->setConfigOverrideLanguage($language_manager->getLanguage('nl'));
    $form_display_storage->resetCache();
    $config_factory->reset();

    // The third party settings to verify.
    $form_translated_third_party_settings = [
      'field_third_party_settings' => [
        'widget_third_party_settings' => [
          'allowed_formats' => [
            'hide_help' => '1',
            'hide_guidelines' => '1',
          ],
          'maxlength' => [
            'maxlength_js' => 180,
            'maxlength_js_label' => 'De inhoud is beperkt tot @limit tekens, resterend: <strong>@remaining</strong>',
            'maxlength_js_enforce' => FALSE,
          ],
        ],
      ],
    ];

    // Assert the third party settings are correctly configured for the
    // maxlength module.
    foreach ($form_translated_third_party_settings as $field_name => $settings) {
      $form_display_component = $form_display_storage->load('node.test_bundle.default')->getComponent($field_name);
      static::assertSame($form_display_component['third_party_settings'], $settings['widget_third_party_settings'], sprintf('The third party settings of the %s custom field are not correctly configured.', $field_name));
    }

    // Add third party settings for the field_third_party_settings field.
    $field_storage = $field_storage_storage->load('node.field_third_party_settings');
    $field_storage->setThirdPartySetting('yaml_bundles_test', 'storage_remove_test', 'remove');
    $field_storage->save();
    $field_instance = $field_instance_storage->load('node.test_bundle.field_third_party_settings');
    $field_instance->setThirdPartySetting('yaml_bundles_test', 'field_remove_test', 'remove');
    $field_instance->save();

    // Recreate the bundles from the yaml_bundles.bundle plugins.
    $bundle_creator->createBundles();

    // Assert the custom settings are removed.
    foreach ($field_third_party_settings as $field_name => $settings) {
      $field_storage = $field_storage_storage->load('node.field_third_party_settings');
      $modules = $field_storage->getThirdPartyProviders();
      foreach ($modules as $module) {
        $settings = $field_storage->getThirdPartySettings($module);
        static::assertArrayHasKey($module, $field_third_party_settings[$field_name]['storage_third_party_settings'], sprintf('The third party settings of the %s custom field storage are not correctly configured.', $field_name));
        static::assertSame($field_third_party_settings[$field_name]['storage_third_party_settings'][$module], $settings, sprintf('The third party settings of the %s custom field storage are not correctly configured.', $field_name));
      }
      $field_instance = $field_instance_storage->load('node.test_bundle.field_third_party_settings');
      $modules = $field_instance->getThirdPartyProviders();
      foreach ($modules as $module) {
        $settings = $field_instance->getThirdPartySettings($module);
        static::assertArrayHasKey($module, $field_third_party_settings[$field_name]['field_third_party_settings'], sprintf('The third party settings of the %s custom field instance are not correctly configured.', $field_name));
        static::assertSame($field_third_party_settings[$field_name]['field_third_party_settings'][$module], $settings, sprintf('The third party settings of the %s custom field instance are not correctly configured.', $field_name));
      }
    }
  }

}
