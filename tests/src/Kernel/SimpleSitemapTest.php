<?php

namespace Drupal\Tests\yaml_bundles\Kernel;

use Drupal\node\NodeTypeInterface;

/**
 * Tests the third party settings of yaml_bundles.bundle plugins.
 *
 * @group yaml_bundles
 */
class SimpleSitemapTest extends YamlBundlesKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'datetime',
    'entity_reference_revisions',
    'field',
    'file',
    'filter',
    'image',
    'language',
    'link',
    'media',
    'media_library',
    'node',
    'options',
    'paragraphs',
    'simple_sitemap',
    'system',
    'text',
    'user',
    'views',
    'yaml_bundles',
    'yaml_bundles_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create a sitemap variant.
    $this->installSchema('simple_sitemap', ['simple_sitemap']);
    $this->installConfig(['simple_sitemap']);
    $this->container->get('entity_type.manager')->getStorage('simple_sitemap')->create([
      'id' => 'test',
      'type' => 'default_hreflang',
    ])->save();

    // Enable sitemaps for nodes.
    $this->container->get('simple_sitemap.entity_manager')->enableEntityType('node');
  }

  /**
   * Tests the creation of a bundle using yaml_bundles.bundle plugins.
   */
  public function testSiteMapSettings(): void {
    $entity_type_manager = $this->container->get('entity_type.manager');
    $sitemap_manager = $this->container->get('simple_sitemap.entity_manager');
    $bundle_creator = $this->container->get('yaml_bundles.bundle_creator');

    $node_type_storage = $entity_type_manager->getStorage('node_type');

    // Create the bundles from the yaml_bundles.bundle plugins.
    $bundle_creator->createBundles();

    // Assert the node type is created.
    $node_type = $node_type_storage->load('test_bundle');
    static::assertInstanceOf(NodeTypeInterface::class, $node_type);

    // Assert the simple sitemap settings are correctly configured.
    static::assertSame($sitemap_manager->getBundleSettings('node', 'test_bundle')['default'], [
      'index' => TRUE,
      'priority' => isset($settings['priority']) ? number_format($settings['priority'], 1) : '0.5',
      'changefreq' => 'weekly',
      'include_images' => FALSE,
    ], sprintf('The simple sitemap settings of the %s bundle are not correctly configured.', 'test_bundle'));
  }

}
