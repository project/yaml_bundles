<?php

namespace Drupal\Tests\yaml_bundles\Kernel;

use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\node\NodeTypeInterface;

/**
 * Tests the creation of field groups from yaml_bundles.bundle plugins.
 *
 * @group yaml_bundles
 */
class FieldGroupTest extends YamlBundlesKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'content_translation',
    'datetime',
    'entity_reference_revisions',
    'field',
    'field_group',
    // The field_group_markup module currently needs a patch to provide the
    // correct schema for the markup plugin.
    // @see https://www.drupal.org/project/field_group_markup/issues/3347747
    'field_group_markup',
    'file',
    'filter',
    'image',
    'language',
    'link',
    'media',
    'media_library',
    'node',
    'options',
    'paragraphs',
    'system',
    'text',
    'user',
    'views',
    'yaml_bundles',
    'yaml_bundles_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Enable the Dutch language.
    ConfigurableLanguage::createFromLangcode('nl')->save();
  }

  /**
   * Tests the creation of a bundle using yaml_bundles.bundle plugins.
   */
  public function testBundleFieldGroups(): void {
    $entity_type_manager = $this->container->get('entity_type.manager');
    $config_factory = $this->container->get('config.factory');
    $language_manager = $this->container->get('language_manager');
    $bundle_creator = $this->container->get('yaml_bundles.bundle_creator');

    $node_type_storage = $entity_type_manager->getStorage('node_type');
    $form_display_storage = $entity_type_manager->getStorage('entity_form_display');
    $view_display_storage = $entity_type_manager->getStorage('entity_view_display');

    // Create the bundles from the yaml_bundles.bundle plugins.
    $bundle_creator->createBundles();

    // Assert the node type is created.
    $node_type = $node_type_storage->load('test_bundle');
    static::assertInstanceOf(NodeTypeInterface::class, $node_type);

    // Assert the group settings are correctly added to the form displays.
    $form_display = $form_display_storage->load('node.test_bundle.default');
    static::assertSame([
      'group_tabs' => [
        'children' => [
          'group_main',
          'group_content',
          'group_settings',
        ],
        'label' => 'Tabs',
        'region' => 'content',
        'parent_name' => '',
        'weight' => 0,
        'format_type' => 'tabs',
        'format_settings' => [
          'classes' => '',
          'show_empty_fields' => FALSE,
          'id' => '',
          'direction' => 'horizontal',
          'width_breakpoint' => 640,
        ],
      ],
      'group_main' => [
        'children' => [
          'title',
          'field_entity_text',
          'field_extra_title',
        ],
        'label' => 'Main',
        'region' => 'content',
        'parent_name' => 'group_tabs',
        'weight' => 0,
        'format_type' => 'tab',
        'format_settings' => [
          'classes' => '',
          'show_empty_fields' => FALSE,
          'id' => '',
          'formatter' => 'closed',
          'description' => '',
          'required_fields' => TRUE,
        ],
      ],
      'group_content' => [
        'children' => [
          'group_description',
          'field_date',
          'field_end_date',
          'group_meta',
          'field_summary',
          'field_body',
          'field_link',
          'field_blocks',
          'group_media',
        ],
        'label' => 'Content',
        'region' => 'content',
        'parent_name' => 'group_tabs',
        'weight' => 1,
        'format_type' => 'tab',
        'format_settings' => [
          'classes' => '',
          'show_empty_fields' => FALSE,
          'id' => '',
          'formatter' => 'closed',
          'description' => '',
          'required_fields' => TRUE,
        ],
      ],
      'group_description' => [
        'children' => [],
        'label' => 'Description',
        'region' => 'content',
        'parent_name' => 'group_content',
        'weight' => 0,
        'format_type' => 'markup',
        'format_settings' => [
          'classes' => '',
          'show_empty_fields' => TRUE,
          'id' => '',
          'markup' => [
            'value' => 'This is a description for the content group.',
            'format' => 'plain_text',
          ],
        ],
      ],
      'group_meta' => [
        'children' => [],
        'label' => 'Meta',
        'region' => 'content',
        'parent_name' => 'group_content',
        'weight' => 3,
        'format_type' => 'markup',
        'format_settings' => [
          'classes' => '',
          'show_empty_fields' => TRUE,
          'id' => '',
          'markup' => [
            'value' => 'This is a description for the meta group.',
            'format' => 'full_html',
          ],
        ],
      ],
      'group_media' => [
        'children' => [
          'field_media',
          'field_image',
        ],
        'label' => 'Media',
        'region' => 'content',
        'parent_name' => 'group_content',
        'weight' => 8,
        'format_type' => 'details',
        'format_settings' => [
          'classes' => '',
          'show_empty_fields' => FALSE,
          'id' => '',
          'open' => TRUE,
          'description' => '',
          'required_fields' => TRUE,
        ],
      ],
      'group_settings' => [
        'children' => [
          'field_category',
          'field_status',
          'field_count',
        ],
        'label' => 'Settings',
        'region' => 'content',
        'parent_name' => 'group_tabs',
        'weight' => 2,
        'format_type' => 'tab',
        'format_settings' => [
          'classes' => '',
          'show_empty_fields' => FALSE,
          'id' => '',
          'formatter' => 'closed',
          'description' => '',
          'required_fields' => TRUE,
        ],
      ],
    ], $form_display->getThirdPartySettings('field_group'), 'The group settings are not correctly added to the default form display.');
    $form_display = $form_display_storage->load('node.test_bundle.short');
    static::assertSame([
      'group_tabs' => [
        'children' => [
          'group_main',
          'group_content',
          'group_settings',
        ],
        'label' => 'Tabs',
        'region' => 'content',
        'parent_name' => '',
        'weight' => 0,
        'format_type' => 'tabs',
        'format_settings' => [
          'classes' => '',
          'show_empty_fields' => FALSE,
          'id' => '',
          'direction' => 'horizontal',
          'width_breakpoint' => 640,
        ],
      ],
      'group_main' => [
        'children' => [
          'title',
          'field_entity_text',
          'field_extra_title',
        ],
        'label' => 'Main',
        'region' => 'content',
        'parent_name' => 'group_tabs',
        'weight' => 0,
        'format_type' => 'tab',
        'format_settings' => [
          'classes' => '',
          'show_empty_fields' => FALSE,
          'id' => '',
          'formatter' => 'closed',
          'description' => '',
          'required_fields' => TRUE,
        ],
      ],
    ], $form_display->getThirdPartySettings('field_group'), 'The group settings are not correctly added to the short form display.');

    // Assert the group settings are correctly added to the view display.
    $view_display = $view_display_storage->load('node.test_bundle.default');
    static::assertSame([
      'group_description' => [
        'children' => [],
        'label' => 'Description',
        'parent_name' => 'group_content',
        'region' => 'content',
        'weight' => 0,
        'format_type' => 'markup',
        'format_settings' => [
          'classes' => '',
          'show_empty_fields' => TRUE,
          'id' => '',
          'markup' => [
            'value' => 'This is a description for the content group.',
            'format' => 'plain_text',
          ],
        ],
      ],
    ], $view_display->getThirdPartySettings('field_group'), 'The group settings are not correctly added to the default view display.');
    $view_display = $view_display_storage->load('node.test_bundle.teaser');
    static::assertSame([], $view_display->getThirdPartySettings('field_group'), 'The group settings are not correctly added to the teaser view display.');

    // Assert the group settings are correctly translated.
    $language_manager->setConfigOverrideLanguage($language_manager->getLanguage('nl'));
    $config_factory->reset();

    // Assert the group setting translations are correctly added to the form
    // display.
    $form_display = $form_display_storage->load('node.test_bundle.default');
    static::assertSame([
      'group_tabs' => [
        'children' => [
          'group_main',
          'group_content',
          'group_settings',
        ],
        'label' => 'Tabs',
        'region' => 'content',
        'parent_name' => '',
        'weight' => 0,
        'format_type' => 'tabs',
        'format_settings' => [
          'classes' => '',
          'show_empty_fields' => FALSE,
          'id' => '',
          'direction' => 'horizontal',
          'width_breakpoint' => 640,
        ],
      ],
      'group_main' => [
        'children' => [
          'title',
          'field_entity_text',
          'field_extra_title',
        ],
        'label' => 'Algemeen',
        'region' => 'content',
        'parent_name' => 'group_tabs',
        'weight' => 0,
        'format_type' => 'tab',
        'format_settings' => [
          'classes' => '',
          'show_empty_fields' => FALSE,
          'id' => '',
          'formatter' => 'closed',
          'description' => '',
          'required_fields' => TRUE,
        ],
      ],
      'group_content' => [
        'children' => [
          'group_description',
          'field_date',
          'field_end_date',
          'group_meta',
          'field_summary',
          'field_body',
          'field_link',
          'field_blocks',
          'group_media',
        ],
        'label' => 'Inhoud',
        'region' => 'content',
        'parent_name' => 'group_tabs',
        'weight' => 1,
        'format_type' => 'tab',
        'format_settings' => [
          'classes' => '',
          'show_empty_fields' => FALSE,
          'id' => '',
          'formatter' => 'closed',
          'description' => '',
          'required_fields' => TRUE,
        ],
      ],
      'group_description' => [
        'children' => [],
        'label' => 'Beschrijving',
        'region' => 'content',
        'parent_name' => 'group_content',
        'weight' => 0,
        'format_type' => 'markup',
        'format_settings' => [
          'classes' => '',
          'show_empty_fields' => TRUE,
          'id' => '',
          'markup' => [
            'value' => 'Dit is een beschrijving voor de inhoud groep.',
            'format' => 'plain_text',
          ],
        ],
      ],
      'group_meta' => [
        'children' => [],
        'label' => 'Meta',
        'region' => 'content',
        'parent_name' => 'group_content',
        'weight' => 3,
        'format_type' => 'markup',
        'format_settings' => [
          'classes' => '',
          'show_empty_fields' => TRUE,
          'id' => '',
          'markup' => [
            'value' => 'Dit is een beschrijving voor de meta groep.',
            'format' => 'full_html',
          ],
        ],
      ],
      'group_media' => [
        'children' => [
          'field_media',
          'field_image',
        ],
        'label' => 'Media',
        'region' => 'content',
        'parent_name' => 'group_content',
        'weight' => 8,
        'format_type' => 'details',
        'format_settings' => [
          'classes' => '',
          'show_empty_fields' => FALSE,
          'id' => '',
          'open' => TRUE,
          'description' => '',
          'required_fields' => TRUE,
        ],
      ],
      'group_settings' => [
        'children' => [
          'field_category',
          'field_status',
          'field_count',
        ],
        'label' => 'Instellingen',
        'region' => 'content',
        'parent_name' => 'group_tabs',
        'weight' => 2,
        'format_type' => 'tab',
        'format_settings' => [
          'classes' => '',
          'show_empty_fields' => FALSE,
          'id' => '',
          'formatter' => 'closed',
          'description' => '',
          'required_fields' => TRUE,
        ],
      ],
    ], $form_display->getThirdPartySettings('field_group'), 'The group settings are not correctly translated for the default form display.');
    $form_display = $form_display_storage->load('node.test_bundle.short');
    static::assertSame([
      'group_tabs' => [
        'children' => [
          'group_main',
          'group_content',
          'group_settings',
        ],
        'label' => 'Tabs',
        'region' => 'content',
        'parent_name' => '',
        'weight' => 0,
        'format_type' => 'tabs',
        'format_settings' => [
          'classes' => '',
          'show_empty_fields' => FALSE,
          'id' => '',
          'direction' => 'horizontal',
          'width_breakpoint' => 640,
        ],
      ],
      'group_main' => [
        'children' => [
          'title',
          'field_entity_text',
          'field_extra_title',
        ],
        'label' => 'Algemeen',
        'region' => 'content',
        'parent_name' => 'group_tabs',
        'weight' => 0,
        'format_type' => 'tab',
        'format_settings' => [
          'classes' => '',
          'show_empty_fields' => FALSE,
          'id' => '',
          'formatter' => 'closed',
          'description' => '',
          'required_fields' => TRUE,
        ],
      ],
    ], $form_display->getThirdPartySettings('field_group'), 'The group settings are not correctly translated for the short form display.');

    // Assert the group settings are correctly added to the view display.
    $view_display = $view_display_storage->load('node.test_bundle.default');
    static::assertSame([
      'group_description' => [
        'children' => [],
        'label' => 'Beschrijving',
        'parent_name' => 'group_content',
        'region' => 'content',
        'weight' => 0,
        'format_type' => 'markup',
        'format_settings' => [
          'classes' => '',
          'show_empty_fields' => TRUE,
          'id' => '',
          'markup' => [
            'value' => 'Dit is een beschrijving voor de inhoud groep.',
            'format' => 'plain_text',
          ],
        ],
      ],
    ], $view_display->getThirdPartySettings('field_group'), 'The group settings are not correctly translated for the default view display');
    $view_display = $view_display_storage->load('node.test_bundle.teaser');
    static::assertSame([], $view_display->getThirdPartySettings('field_group'), 'The group settings are not correctly translated for the teaser view display');

  }

}
