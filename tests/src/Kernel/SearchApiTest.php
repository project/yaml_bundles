<?php

namespace Drupal\Tests\yaml_bundles\Kernel;

use Drupal\node\NodeTypeInterface;

/**
 * Tests the Search API support of yaml_bundles.bundle plugins.
 *
 * @group yaml_bundles
 */
class SearchApiTest extends YamlBundlesKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'datetime',
    'entity_reference_revisions',
    'field',
    'file',
    'filter',
    'image',
    'language',
    'link',
    'media',
    'media_library',
    'node',
    'options',
    'paragraphs',
    'search_api',
    'search_api_solr',
    'system',
    'text',
    'user',
    'views',
    'workflows',
    'yaml_bundles',
    'yaml_bundles_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('search_api_task');
    $this->installConfig('search_api');

    /** @var \Drupal\search_api\Utility\PluginHelperInterface $plugin_helper */
    $plugin_helper = $this->container->get('search_api.plugin_helper');
    /** @var \Drupal\search_api\Utility\FieldsHelperInterface $fields_helper */
    $fields_helper = $this->container->get('search_api.fields_helper');

    // Create a search API index.
    /** @var \Drupal\search_api\IndexInterface $search_index */
    $search_index = $this->container->get('entity_type.manager')->getStorage('search_api_index')->create([
      'id' => 'default',
      'name' => 'Default',
    ]);
    $search_index->addDatasource($plugin_helper->createDatasourcePlugin($search_index, 'entity:node'));
    $search_index->addProcessor($plugin_helper->createProcessorPlugin($search_index, 'type_boost'));
    $search_index->addProcessor($plugin_helper->createProcessorPlugin($search_index, 'solr_boost_more_recent'));
    $search_index->addProcessor($plugin_helper->createProcessorPlugin($search_index, 'rendered_item'));
    $search_index->addField($fields_helper->createField($search_index, 'rendered_item', [
      'type' => 'text',
      'property_path' => 'rendered_item',
      'configuration' => [
        'roles' => ['anonymous'],
        'view_mode' => [],
      ],
    ]));
    $search_index->save();
  }

  /**
   * Tests the creation of a bundle using yaml_bundles.bundle plugins.
   */
  public function testSearchApiSettings(): void {
    $entity_type_manager = $this->container->get('entity_type.manager');
    $bundle_creator = $this->container->get('yaml_bundles.bundle_creator');

    $node_type_storage = $entity_type_manager->getStorage('node_type');

    // Create the bundles from the yaml_bundles.bundle plugins.
    $bundle_creator->createBundles();

    // Assert the node type is created.
    $node_type = $node_type_storage->load('test_bundle');
    static::assertInstanceOf(NodeTypeInterface::class, $node_type);

    // Load the updated search index.
    $search_index = $this->container->get('entity_type.manager')->getStorage('search_api_index')->load('default');

    // Assert the bundle is added to index with the correct boost.
    $configuration = $search_index->getProcessor('type_boost')->getConfiguration();
    static::assertSame(1.5, $configuration['boosts']['entity:node']['bundle_boosts']['test_bundle'], 'The search index boost is not set correctly.');

    $fields = $search_index->getFields();

    // Assert the full view mode is added to the index for rendered items.
    static::assertSame('full', $fields['rendered_item']->getConfiguration()['view_mode']['entity:node']['test_bundle'], 'The search index rendered item is not set correctly.');

    // Assert the fields are added to the index.
    $field_settings = [
      'field_blocks' => FALSE,
      'field_body' => [
        'label' => 'Text',
        'type' => 'string',
        'property_path' => 'field_body',
      ],
      'field_body_fulltext' => [
        'label' => 'Text (fulltext)',
        'type' => 'solr_text_omit_norms',
        'property_path' => 'field_body',
        'boost' => 1,
      ],
      'field_body_ngram' => [
        'label' => 'Text (ngram)',
        'type' => 'solr_text_custom_omit_norms:edge',
        'property_path' => 'field_body',
        'boost' => 1,
      ],
      'field_category' => [
        'label' => 'Category',
        'type' => 'string',
        'property_path' => 'field_category',
      ],
      'field_count' => [
        'label' => 'Count',
        'type' => 'integer',
        'property_path' => 'field_count',
      ],
      'field_date' => [
        'label' => 'Date',
        'type' => 'date',
        'property_path' => 'field_date',
      ],
      'field_extra_title' => [
        'label' => 'Extra title',
        'type' => 'string',
        'property_path' => 'field_extra_title',
      ],
      'field_extra_title_fulltext' => [
        'label' => 'Extra title (fulltext)',
        'type' => 'solr_text_omit_norms',
        'property_path' => 'field_extra_title',
        'boost' => 10,
      ],
      'field_extra_title_ngram' => [
        'label' => 'Extra title (ngram)',
        'type' => 'solr_text_custom_omit_norms:edge',
        'property_path' => 'field_extra_title',
        'boost' => 5,
      ],
      'field_image' => [
        'label' => 'Image',
        'type' => 'integer',
        'property_path' => 'field_image',
      ],
      'field_link' => [
        'label' => 'Link',
        'type' => 'string',
        'property_path' => 'field_link',
      ],
      'field_media' => FALSE,
      'field_status' => [
        'label' => 'Status',
        'type' => 'boolean',
        'property_path' => 'field_status',
      ],
      'field_summary' => [
        'label' => 'Summary',
        'type' => 'string',
        'property_path' => 'field_summary',
      ],
      'field_summary_fulltext' => [
        'label' => 'Summary (fulltext)',
        'type' => 'solr_text_omit_norms',
        'property_path' => 'field_summary',
        'boost' => 5,
      ],
      'field_summary_ngram' => [
        'label' => 'Summary (ngram)',
        'type' => 'solr_text_custom_omit_norms:edge',
        'property_path' => 'field_summary',
        'boost' => 2.5,
      ],
      'langcode' => [
        'label' => 'Language',
        'type' => 'string',
        'property_path' => 'langcode',
      ],
      'title' => [
        'label' => 'Admin title',
        'type' => 'string',
        'property_path' => 'title',
      ],
      'title_fulltext' => [
        'label' => 'Admin title (fulltext)',
        'type' => 'solr_text_omit_norms',
        'property_path' => 'title',
        'boost' => 20,
      ],
      'title_ngram' => [
        'label' => 'Admin title (ngram)',
        'type' => 'solr_text_custom_omit_norms:edge',
        'property_path' => 'title',
        'boost' => 10,
      ],
    ];

    /** @var \Drupal\search_api\Item\FieldInterface[] $fields */
    foreach ($field_settings as $field_name => $field_settings) {
      if (!$field_settings) {
        static::assertArrayNotHasKey($field_name, $fields, sprintf('The search index field %s is not set correctly.', $field_name));
      }
      else {
        static::assertArrayHasKey($field_name, $fields, sprintf('The search index field %s is not set correctly.', $field_name));
        static::assertSame($fields[$field_name]->getLabel(), $field_settings['label'], sprintf('The label for search index field %s is not set correctly.', $field_name));
        static::assertSame($fields[$field_name]->getType(), $field_settings['type'], sprintf('The type for search index field %s is not set correctly.', $field_name));
        static::assertSame($fields[$field_name]->getPropertyPath(), $field_settings['property_path'], sprintf('The property path for search index field %s is not set correctly.', $field_name));
        if (isset($field_settings['boost'])) {
          static::assertSame($fields[$field_name]->getBoost(), (float) $field_settings['boost'], sprintf('The boost for search index field %s is not set correctly.', $field_name));
        }
        else {
          static::assertSame(1.0, $fields[$field_name]->getBoost(), sprintf('The boost for search index field %s is not set correctly.', $field_name));
        }
      }
    }

    // Assert the date field is boosted correctly.
    $date_boost_configuration = $search_index->getProcessor('solr_boost_more_recent')->getConfiguration();
    static::assertSame([
      'boost' => 0.5,
      'resolution' => 'NOW',
      'm' => '3.16e-11',
      'a' => 0.1,
      'b' => 0.05,
    ], $date_boost_configuration['boosts']['field_date'], 'The search index date field is not boosted correctly.');

    // Assert the end date field does not contain a date boost.
    static::assertArrayNotHasKey('field_end_date', $date_boost_configuration['boosts'], 'The search index end date field is not boosted correctly.');
  }

}
