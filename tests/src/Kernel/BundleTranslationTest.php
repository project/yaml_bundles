<?php

namespace Drupal\Tests\yaml_bundles\Kernel;

use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\node\NodeTypeInterface;

/**
 * Tests the creation of bundle translations yaml_bundles.bundle plugins.
 *
 * @group yaml_bundles
 */
class BundleTranslationTest extends YamlBundlesKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'content_translation',
    'datetime',
    'entity_reference_revisions',
    'field',
    'file',
    'filter',
    'image',
    'language',
    'link',
    'media',
    'media_library',
    'node',
    'options',
    'paragraphs',
    'system',
    'text',
    'user',
    'views',
    'yaml_bundles',
    'yaml_bundles_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Enable the Dutch language.
    ConfigurableLanguage::createFromLangcode('nl')->save();
  }

  /**
   * Tests the creation of a bundle using yaml_bundles.bundle plugins.
   */
  public function testBundleTranslations(): void {
    $entity_type_manager = $this->container->get('entity_type.manager');
    $entity_field_manager = $this->container->get('entity_field.manager');
    $config_factory = $this->container->get('config.factory');
    $language_manager = $this->container->get('language_manager');
    $bundle_creator = $this->container->get('yaml_bundles.bundle_creator');

    $node_type_storage = $entity_type_manager->getStorage('node_type');
    $form_display_storage = $entity_type_manager->getStorage('entity_form_display');

    // Create the bundles from the yaml_bundles.bundle plugins.
    $bundle_creator->createBundles();

    // Assert the node type is created.
    $node_type = $node_type_storage->load('test_bundle');
    static::assertInstanceOf(NodeTypeInterface::class, $node_type);

    // Assert the node type properties.
    static::assertSame('Test bundle', $node_type->get('name'));
    static::assertSame('A description for the test bundle.', $node_type->get('description'));

    // Assert the node type config is not translated since the test is not
    // multilingual. We leave the lanuage override to also assert the fields are
    // returned in the correct language.
    $language_manager->setConfigOverrideLanguage($language_manager->getLanguage('nl'));
    $node_type_storage->resetCache();
    $config_factory->reset();
    $node_type = $node_type_storage->load('test_bundle');
    static::assertSame('Test bundel', $node_type->get('name'), 'The node type label is not translated.');

    // Assert the node type content translation settings are not created since
    // the site is not multilingual. The config factory always returns an
    // ImmutableConfig object but if doesn't exist in configuration yet, isNew()
    // is true. In this case it should be false.
    static::assertFalse($this->config('language.content_settings.node.test_bundle')->isNew(), 'The node type content translation settings are not created.');

    $entity_field_manager->clearCachedFieldDefinitions();
    $storage_definitions = $entity_field_manager->getFieldStorageDefinitions('node');
    $field_definitions = $entity_field_manager->getFieldDefinitions('node', 'test_bundle');

    // Assert the field labels and descriptions are loaded in the correct
    // language.
    $translations = [
      'langcode' => [
        'label' => 'Taal',
      ],
      'title' => [
        'label' => 'Administratieve titel',
        'description' => 'Dit is een beschrijving voor de titel.',
      ],
      'field_extra_title' => [
        'label' => 'Extra titel',
        'form_displays' => ['default', 'short'],
        'widget_settings' => [
          'size' => 60,
          'placeholder' => 'Dit is een placeholder.',
        ],
      ],
      'field_summary' => [
        'label' => 'Samenvatting',
        'description' => 'Dit is een beschrijving voor de samenvatting.',
      ],
      'field_body' => [
        'label' => 'Tekst',
      ],
      'field_media' => [
        'label' => 'Media',
      ],
      'field_image' => [
        'label' => 'Afbeelding',
      ],
      'field_category' => [
        'label' => 'Categorie',
        'storage_settings' => [
          'allowed_values' => [
            'category_1' => 'Categorie 1',
            'category_2' => 'Categorie 2',
          ],
          'allowed_values_function' => '',
        ],
      ],
      'field_status' => [
        'label' => 'Status',
      ],
      'field_link' => [
        'label' => 'Link',
      ],
      'field_date' => [
        'label' => 'Datum',
      ],
      'field_count' => [
        'label' => 'Aantal',
      ],
      'field_blocks' => [
        'label' => 'Blokken',
        'form_displays' => ['default', 'short'],
        'widget_settings' => [
          'title' => 'Blok',
          'title_plural' => 'Blokken',
          'edit_mode' => 'open',
          'closed_mode' => 'summary',
          'autocollapse' => 'none',
          'closed_mode_threshold' => 0,
          'add_mode' => 'button',
          'form_display_mode' => 'default',
          'default_paragraph_type' => '_none',
          'features' => [
            'add_above' => '0',
            'collapse_edit_all' => '0',
            'duplicate' => '0',
          ],
        ],
      ],
    ];
    foreach ($translations as $field_name => $translation) {
      // Assert the field storage settings are loaded in the correct language.
      if (isset($translation['storage_settings'])) {
        static::assertSame($storage_definitions[$field_name]->getSettings(), $translation['storage_settings'], sprintf('The storage settings of the %s field are not translated.', $field_name));
      }

      // Assert the field labels and descriptions are loaded in the correct
      // language.
      static::assertSame($field_definitions[$field_name]->getLabel(), $translation['label'], sprintf('The label of the %s field is not translated.', $field_name));
      if (isset($translation['description'])) {
        static::assertSame($field_definitions[$field_name]->getDescription(), $translation['description'], sprintf('The description of the %s field is not translated.', $field_name));
      }
      else {
        static::assertEmpty($field_definitions[$field_name]->getDescription(), sprintf('The description of the %s field is not empty.', $field_name));
      }

      // Assert the widget settings are loaded in the correct language.
      foreach ($translation['form_displays'] ?? [] as $display_id) {
        $form_display_component = $form_display_storage->load('node.test_bundle.' . $display_id)->getComponent($field_name);
        static::assertSame($form_display_component['settings'], $translation['widget_settings'], sprintf('The widget settings of the %s custom field are not translated.', $field_name));
      }
    }
  }

}
