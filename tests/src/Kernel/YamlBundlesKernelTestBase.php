<?php

namespace Drupal\Tests\yaml_bundles\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\Tests\media\Traits\MediaTypeCreationTrait;

/**
 * Adds a base class for the YAML bundles kernel tests.
 *
 * @group yaml_bundles
 */
class YamlBundlesKernelTestBase extends KernelTestBase {

  use MediaTypeCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'datetime',
    'entity_reference_revisions',
    'field',
    'file',
    'filter',
    'image',
    'language',
    'link',
    'media',
    'media_library',
    'node',
    'options',
    'path',
    'path_alias',
    'pathauto',
    'paragraphs',
    'system',
    'text',
    'token',
    'user',
    'views',
    'yaml_bundles',
    'yaml_bundles_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installSchema('system', ['sequences']);
    $this->installSchema('file', ['file_usage']);
    $this->installEntitySchema('file');
    $this->installEntitySchema('media');
    $this->installEntitySchema('node');
    $this->installEntitySchema('paragraph');
    $this->installEntitySchema('path_alias');
    $this->installEntitySchema('user');
    $this->installSchema('node', ['node_access']);
    $this->installConfig([
      'filter',
      'node',
      'pathauto',
      'system',
    ]);

    // Create the image media type.
    $this->createMediaType('image', [
      'id' => 'image',
      'label' => 'Image',
    ]);

    // Create the logo media type.
    $this->createMediaType('image', [
      'id' => 'logo',
      'label' => 'Logo',
    ]);

    // Create the block paragraph type.
    $paragraphs_type = ParagraphsType::create([
      'id' => 'block',
      'label' => 'Block',
    ]);
    $paragraphs_type->save();
  }

}
