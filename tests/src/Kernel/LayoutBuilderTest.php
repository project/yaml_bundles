<?php

namespace Drupal\Tests\yaml_bundles\Kernel;

use Drupal\node\NodeTypeInterface;

/**
 * Tests the layout builder support of yaml_bundles.bundle plugins.
 *
 * @group yaml_bundles
 */
class LayoutBuilderTest extends YamlBundlesKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'datetime',
    'entity_reference_revisions',
    'field',
    'file',
    'filter',
    'image',
    'language',
    'layout_builder',
    'layout_discovery',
    'link',
    'media',
    'media_library',
    'node',
    'options',
    'paragraphs',
    'system',
    'text',
    'user',
    'views',
    'yaml_bundles',
    'yaml_bundles_test',
  ];

  /**
   * Tests the creation of a bundle using yaml_bundles.bundle plugins.
   */
  public function testLayoutBuilderSettings(): void {
    $entity_type_manager = $this->container->get('entity_type.manager');
    $entity_field_manager = $this->container->get('entity_field.manager');
    $bundle_creator = $this->container->get('yaml_bundles.bundle_creator');

    $node_type_storage = $entity_type_manager->getStorage('node_type');
    $view_display_storage = $entity_type_manager->getStorage('entity_view_display');

    // Create the bundles from the yaml_bundles.bundle plugins.
    $bundle_creator->createBundles();

    // Assert the node type is created.
    $node_type = $node_type_storage->load('test_bundle');
    static::assertInstanceOf(NodeTypeInterface::class, $node_type);

    $field_definitions = $entity_field_manager->getFieldDefinitions('node', 'test_bundle');

    // Assert the layout builder field is created.
    static::assertArrayHasKey('layout_builder__layout', $field_definitions, 'The layout_builder__layout field is not created.');
    static::assertSame('Layout', $field_definitions['layout_builder__layout']->getLabel(), 'The label of the layout_builder__layout field is not correct.');
    static::assertSame('layout_section', $field_definitions['layout_builder__layout']->getType(), 'The type of the layout_builder__layout field is not correct.');

    // Assert layout builder is enabled for the default view display.
    /** @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface $view_display */
    $view_display = $view_display_storage->load('node.test_bundle.default');
    static::assertTrue($view_display->getThirdPartySettings('layout_builder')['enabled'], 'The layout builder is not enabled for the default view display.');
    static::assertTrue($view_display->getThirdPartySettings('layout_builder')['allow_custom'], 'The layout builder is not configurable for the default view display.');
  }

}
