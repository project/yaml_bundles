<?php

namespace Drupal\Tests\yaml_bundles\Kernel;

use Drupal\Core\Entity\Display\EntityFormDisplayInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\field\FieldConfigInterface;
use Drupal\field\FieldStorageConfigInterface;
use Drupal\node\NodeTypeInterface;

/**
 * Tests the creation of bundle using yaml_bundles.bundle plugins.
 *
 * @group yaml_bundles
 */
class CreateBundleTest extends YamlBundlesKernelTestBase {

  /**
   * Tests the creation of a bundle using yaml_bundles.bundle plugins.
   */
  public function testCreateBundle(): void {
    $entity_type_manager = $this->container->get('entity_type.manager');
    $language_manager = $this->container->get('language_manager');
    $bundle_creator = $this->container->get('yaml_bundles.bundle_creator');

    $node_type_storage = $entity_type_manager->getStorage('node_type');
    $form_display_storage = $entity_type_manager->getStorage('entity_form_display');
    $view_display_storage = $entity_type_manager->getStorage('entity_view_display');

    // Create the bundles from the yaml_bundles.bundle plugins.
    $bundle_creator->createBundles();

    // Assert the node type is created.
    $node_type = $node_type_storage->load('test_bundle');
    static::assertInstanceOf(NodeTypeInterface::class, $node_type);

    // Assert the node type properties.
    static::assertSame('Test bundle', $node_type->get('name'), 'The name of the test bundle is not correct.');
    static::assertSame('A description for the test bundle.', $node_type->get('description'), 'The description of the test bundle is not correct.');
    static::assertSame('en', $node_type->get('langcode'), 'The langcode of the test bundle is not correct.');
    static::assertSame('Help text for the test bundle.', $node_type->get('help'), 'The help text of the test bundle is not correct.');
    static::assertTrue($node_type->get('new_revision'), 'The new revision setting of the test bundle is not correct.');
    static::assertSame(0, $node_type->get('preview_mode'), 'The preview mode of the test bundle is not correct.');
    static::assertFalse($node_type->get('display_submitted'), 'The display submitted setting of the test bundle is not correct.');

    // Assert the pathauto pattern is created.
    $pattern = $entity_type_manager->getStorage('pathauto_pattern')->load('node_test_bundle');
    static::assertSame('test-bundle/[node:title]', $pattern->getPattern(), 'The pathauto pattern of the test bundle is not correct.');

    // Assert the node type config is not translated since the test is not
    // multilingual. We leave the lanuage override to also assert the fields are
    // returned in the correct language.
    $language_manager->setConfigOverrideLanguage($language_manager->getLanguage('nl'));
    $node_type_storage->resetCache();
    $node_type = $node_type_storage->load('test_bundle');
    static::assertSame('Test bundle', $node_type->get('name'));

    // Assert the node type content translation settings are not created since
    // the site is not multilingual. The config factory always returns an
    // ImmutableConfig object but if doesn't exist in configuration yet, isNew()
    // is true.
    static::assertTrue($this->config('language.content_settings.node.test_bundle')->isNew());

    // Assert the form displays are created.
    $default_form_display = $form_display_storage->load('node.test_bundle.default');
    static::assertInstanceOf(EntityFormDisplayInterface::class, $default_form_display);
    $short_form_display = $form_display_storage->load('node.test_bundle.short');
    static::assertInstanceOf(EntityFormDisplayInterface::class, $short_form_display);

    // Assert the view displays are created.
    $default_view_display = $view_display_storage->load('node.test_bundle.default');
    static::assertInstanceOf(EntityViewDisplayInterface::class, $default_view_display);
    $full_view_display = $view_display_storage->load('node.test_bundle.full');
    static::assertInstanceOf(EntityViewDisplayInterface::class, $full_view_display);
    $teaser_view_display = $view_display_storage->load('node.test_bundle.teaser');
    static::assertInstanceOf(EntityViewDisplayInterface::class, $teaser_view_display);

    // Assert the langcode field is added to the correct form and view displays
    // (including the correct widget and formatter settings).
    $this->assertBaseField(
      'langcode',
      // The base field override settings.
      [
        'label' => 'Language',
      ],
      // The form displays the field should be added to and the field weight in
      // the form displays.
      ['default' => 0, 'short' => 0],
      // The widget settings of the field.
      [
        'type' => 'language_select',
        'settings' => [
          'include_locked' => TRUE,
        ],
      ],
      // The langcode is not added to any view displays.
      [],
      // We don't need to pass the formatter settings.
      [],
    );

    // Assert the title field is added to the correct form and view displays
    // (including the correct widget and formatter settings). Since the title
    // base field is not display configurable for view displays, it should not
    // be added even thought it is configured to be added.
    $this->assertBaseField(
      'title',
      // The base field override settings.
      [
        'label' => 'Admin title',
        'description' => 'This is a description for the title.',
      ],
      // The form displays the field should be added to and the field weight in
      // the form displays.
      ['default' => 1, 'short' => 1],
      // The widget settings of the field.
      [
        'type' => 'string_textfield',
        'weight' => 1,
        'settings' => [
          'size' => 60,
          'placeholder' => '',
        ],
      ],
      // The view displays the field should be added to. The weight should be
      // the same as the weight in Node::baseFieldDefinitions().
      ['default' => -5, 'full' => -5, 'teaser' => -5],
      // The formatter settings of the field. The values should be the same as
      // the display options in Node::baseFieldDefinitions().
      ['type' => 'string', 'settings' => ['link_to_entity' => FALSE]],
    );

    // Assert the extra title field is properly created and has the correct
    // settings.
    $this->assertCustomField(
      'field_extra_title',
      // The field storage settings.
      [
        'type' => 'string',
        'cardinality' => 1,
        'settings' => [
          'max_length' => 255,
          'case_sensitive' => FALSE,
          'is_ascii' => FALSE,
        ],
      ],
      // The field instance settings.
      ['label' => 'Extra title', 'required' => TRUE],
      // The form displays the field should be added to and the field weight in
      // the form display.
      ['default' => 2, 'short' => 2],
      // The widget settings of the field.
      [
        'type' => 'string_textfield',
        'settings' => [
          'size' => 60,
          'placeholder' => 'This is a placeholder.',
        ],
      ],
      // The view displays the field should be added to and the field weight in
      // the view display.
      ['default' => 0, 'full' => 0, 'teaser' => 0],
      // The formatter settings of the field.
      ['type' => 'string', 'settings' => ['link_to_entity' => FALSE]]
    );

    // Assert the summary field is properly created and has the correct
    // settings.
    $this->assertCustomField(
      'field_summary',
      // The field storage settings.
      [
        'type' => 'text_long',
        'cardinality' => 1,
        'settings' => [],
      ],
      // The field instance settings.
      [
        'label' => 'Summary',
        'required' => FALSE,
        'description' => 'This is a description for the summary.',
        'settings' => ['allowed_formats' => ['basic_html']],
      ],
      // The form displays the field should be added to and the field weight in
      // the form display.
      ['default' => 3, 'short' => 3],
      // The widget settings of the field.
      [
        'type' => 'text_textarea',
        'settings' => [
          'rows' => 5,
          'placeholder' => '',
        ],
      ],
      // The view displays the field should be added to and the field weight in
      // the view display.
      ['default' => 1, 'teaser' => 1],
      // The formatter settings of the field.
      ['type' => 'text_default', 'settings' => []]
    );

    // Assert the body field is properly created and has the correct settings.
    $this->assertCustomField(
      'field_body',
      // The field storage settings.
      [
        'type' => 'text_long',
        'cardinality' => 1,
        'settings' => [],
      ],
      // The field instance settings.
      [
        'label' => 'Text',
        'required' => TRUE,
        'settings' => ['allowed_formats' => ['basic_html']],
      ],
      // The form displays the field should be added to and the field weight in
      // the form display.
      ['default' => 4],
      // The widget settings of the field.
      [
        'type' => 'text_textarea',
        'settings' => [
          'rows' => 5,
          'placeholder' => '',
        ],
      ],
      // The view displays the field should be added to and the field weight in
      // the view display.
      ['default' => 2, 'full' => 1],
      // The formatter settings of the field.
      ['type' => 'text_default', 'settings' => []]
    );

    // Assert the media field is properly created and has the correct settings.
    $this->assertCustomField(
      'field_media',
      // The field storage settings.
      [
        'type' => 'entity_reference',
        'cardinality' => 1,
        'settings' => [
          'target_type' => 'media',
        ],
      ],
      // The field instance settings.
      [
        'label' => 'Media',
        'required' => TRUE,
        'settings' => [
          'handler' => 'default:media',
          'handler_settings' => [
            'target_bundles' => [
              0 => 'image',
            ],
          ],
        ],
      ],
      // The form displays the field should be added to and the field weight in
      // the form display.
      ['default' => 5, 'short' => 4],
      // The widget settings of the field.
      [
        'type' => 'media_library_widget',
        'settings' => [
          'media_types' => [
            0 => 'image',
          ],
        ],
      ],
      // The view displays the field should be added to and the field weight in
      // the view display.
      ['default' => 3, 'full' => 2, 'teaser' => 2],
      // The formatter settings of the field.
      [
        'type' => 'entity_reference_entity_view',
        'settings' => [
          'view_mode' => 'default',
          'link' => FALSE,
        ],
      ],
    );

    // Assert the image field is properly created and has the correct settings.
    $this->assertCustomField(
      'field_image',
      // The field storage settings.
      [
        'type' => 'entity_reference',
        'cardinality' => 1,
        'settings' => [
          'target_type' => 'media',
        ],
      ],
      // The field instance settings.
      [
        'label' => 'Image',
        'required' => TRUE,
        'settings' => [
          'handler' => 'default:media',
          'handler_settings' => [
            'target_bundles' => [
              0 => 'image',
              1 => 'logo',
            ],
          ],
        ],
      ],
      // The form displays the field should be added to and the field weight in
      // the form display.
      ['default' => 6, 'short' => 5],
      // The widget settings of the field.
      [
        'type' => 'media_library_widget',
        'settings' => [
          'media_types' => [
            0 => 'image',
            1 => 'logo',
          ],
        ],
      ],
      // The view displays the field should be added to and the field weight in
      // the view display.
      ['default' => 4, 'full' => 3, 'teaser' => 3],
      // The formatter settings of the field.
      [
        'type' => 'entity_reference_entity_view',
        'settings' => [
          'view_mode' => 'default',
          'link' => FALSE,
        ],
      ],
    );

    // Assert the category field is properly created and has the correct
    // settings.
    $this->assertCustomField(
      'field_category',
      // The field storage settings.
      [
        'type' => 'list_string',
        'cardinality' => 1,
        'settings' => [
          'allowed_values' => [
            'category_1' => 'Category 1',
            'category_2' => 'Category 2',
          ],
          'allowed_values_function' => '',
        ],
      ],
      // The field instance settings.
      [
        'label' => 'Category',
        'required' => FALSE,
      ],
      // The form displays the field should be added to and the field weight in
      // the form display.
      ['default' => 7, 'short' => 6],
      // The widget settings of the field.
      [
        'type' => 'options_buttons',
        'settings' => [
        ],
      ],
      // The view displays the field should be added to and the field weight in
      // the view display.
      ['default' => 5, 'full' => 4, 'teaser' => 4],
      // The formatter settings of the field.
      [
        'type' => 'list_default',
        'settings' => [],
      ],
    );

    // Assert the status field is properly created and has the correct settings.
    $this->assertCustomField(
      'field_status',
      // The field storage settings.
      [
        'type' => 'boolean',
        'cardinality' => 1,
        'settings' => [],
      ],
      // The field instance settings.
      [
        'label' => 'Status',
        'required' => FALSE,
        'settings' => [
          'on_label' => 'Yes',
          'off_label' => 'No',
        ],
        'default_value' => [
          0 => [
            'value' => 0,
          ],
        ],
      ],
      // The form displays the field should be added to and the field weight in
      // the form display.
      ['default' => 8, 'short' => 7],
      // The widget settings of the field.
      [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
      ],
      // The view displays the field should be added to and the field weight in
      // the view display.
      ['default' => 6, 'full' => 5, 'teaser' => 5],
      // The formatter settings of the field.
      [
        'type' => 'boolean',
        'label' => 'above',
        'settings' => [
          'format' => 'default',
          'format_custom_false' => '',
          'format_custom_true' => '',
        ],
      ],
    );

    // Assert the link field is properly created and has the correct settings.
    $this->assertCustomField(
      'field_link',
      // The field storage settings.
      [
        'type' => 'link',
        'cardinality' => 2,
        'settings' => [],
      ],
      // The field instance settings.
      [
        'label' => 'Link',
        'required' => TRUE,
        'settings' => [
          'title' => 2,
          'link_type' => 17,
        ],
      ],
      // The form displays the field should be added to and the field weight in
      // the form display.
      ['default' => 9, 'short' => 8],
      // The widget settings of the field.
      [
        'type' => 'link_default',
        'settings' => [
          'placeholder_url' => '',
          'placeholder_title' => '',
        ],
      ],
      // The view displays the field should be added to and the field weight in
      // the view display.
      ['default' => 7, 'full' => 6, 'teaser' => 6],
      // The formatter settings of the field.
      [
        'type' => 'link',
        'settings' => [
          'trim_length' => 80,
          'url_only' => FALSE,
          'url_plain' => FALSE,
          'rel' => '',
          'target' => '',
        ],
      ],
    );

    // Assert the date field is properly created and has the correct settings.
    $this->assertCustomField(
      'field_date',
      // The field storage settings.
      [
        'type' => 'datetime',
        'cardinality' => 1,
        'settings' => [
          'datetime_type' => 'datetime',
        ],
      ],
      // The field instance settings.
      [
        'label' => 'Date',
        'required' => FALSE,
        'settings' => [],
        'default_value' => [
          0 => [
            'default_date_type' => 'now',
            'default_date' => 'now',
          ],
        ],
      ],
      // The form displays the field should be added to and the field weight in
      // the form display.
      ['default' => 10, 'short' => 9],
      // The widget settings of the field.
      [
        'type' => 'datetime_default',
        'settings' => [
        ],
      ],
      // The view displays the field should be added to and the field weight in
      // the view display.
      ['default' => 8, 'full' => 7, 'teaser' => 7],
      // The formatter settings of the field.
      [
        'type' => 'datetime_default',
        'settings' => [
          'timezone_override' => '',
          'format_type' => 'medium',
        ],
      ],
    );

    // Assert the end date field is properly created and has the correct
    // settings.
    $this->assertCustomField(
      'field_end_date',
      // The field storage settings.
      [
        'type' => 'datetime',
        'cardinality' => 1,
        'settings' => [
          'datetime_type' => 'datetime',
        ],
      ],
      // The field instance settings.
      [
        'label' => 'End date',
        'required' => FALSE,
        'settings' => [],
      ],
      // The form displays the field should be added to and the field weight in
      // the form display.
      ['default' => 11, 'short' => 10],
      // The widget settings of the field.
      [
        'type' => 'datetime_default',
        'settings' => [
        ],
      ],
      // The view displays the field should be added to and the field weight in
      // the view display.
      ['default' => 9, 'full' => 8, 'teaser' => 8],
      // The formatter settings of the field.
      [
        'type' => 'datetime_default',
        'settings' => [
          'timezone_override' => '',
          'format_type' => 'medium',
        ],
      ],
    );

    // Assert the blocks field is properly created and has the correct settings.
    $this->assertCustomField(
      'field_blocks',
      // The field storage settings.
      [
        'type' => 'entity_reference_revisions',
        'cardinality' => 5,
        'settings' => [
          'target_type' => 'paragraph',
        ],
      ],
      // The field instance settings.
      [
        'label' => 'Blocks',
        'required' => FALSE,
        'settings' => [
          'handler' => 'default:paragraph',
          'handler_settings' => [
            'target_bundles' => [
              'block' => 'block',
            ],
          ],
        ],
      ],
      // The form displays the field should be added to and the field weight in
      // the form display.
      ['default' => 12, 'short' => 11],
      // The widget settings of the field.
      [
        'type' => 'paragraphs',
        'settings' => [
          'title' => 'Block',
          'title_plural' => 'Blocks',
          'edit_mode' => 'open',
          'closed_mode' => 'summary',
          'autocollapse' => 'none',
          'closed_mode_threshold' => 0,
          'add_mode' => 'button',
          'form_display_mode' => 'default',
          'default_paragraph_type' => '_none',
          'features' => [
            'add_above' => '0',
            'collapse_edit_all' => '0',
            'duplicate' => '0',
          ],
        ],
      ],
      // The view displays the field should be added to and the field weight in
      // the view display.
      ['default' => 10, 'full' => 9, 'teaser' => 9],
      // The formatter settings of the field.
      [
        'type' => 'entity_reference_entity_view',
        'settings' => [
          'view_mode' => 'default',
          'link' => FALSE,
        ],
      ],
    );

    // Assert the count field is properly created and has the correct settings.
    // The field is moved to a different place by the weight property.
    $this->assertCustomField(
      'field_count',
      // The field storage settings.
      [
        'type' => 'integer',
        'cardinality' => 1,
        'settings' => [
          'unsigned' => FALSE,
          'size' => 'normal',
        ],
      ],
      // The field instance settings.
      [
        'label' => 'Count',
        'required' => FALSE,
        'settings' => [
          'min' => 0,
          'max' => 50,
          'prefix' => '',
          'suffix' => '',
        ],
        'default_value' => [
          0 => [
            'value' => 10,
          ],
        ],
      ],
      // The form displays the field should be added to and the field weight in
      // the form display.
      ['default' => 14, 'short' => 13],
      // The widget settings of the field.
      [
        'type' => 'number',
        'settings' => [
          'placeholder' => '',
        ],
      ],
      // The view displays the field should be added to and the field weight in
      // the view display.
      ['default' => 12, 'full' => 11, 'teaser' => 11],
      // The formatter settings of the field.
      [
        'type' => 'number_integer',
        'settings' => [
          'thousand_separator' => '',
          'prefix_suffix' => TRUE,
        ],
      ],
    );

    // Assert the text field from the entity type defaults is properly created
    // and has the correct settings. The field is moved to the last place by the
    // weight property.
    $this->assertCustomField(
      'field_entity_text',
      // The field storage settings.
      [
        'type' => 'string',
        'cardinality' => 1,
        'settings' => [
          'max_length' => 255,
          'case_sensitive' => FALSE,
          'is_ascii' => FALSE,
        ],
      ],
      // The field instance settings.
      ['label' => 'Entity text', 'required' => FALSE],
      // The form displays the field should be added to and the field weight in
      // the form display.
      ['default' => 15, 'short' => 14],
      // The widget settings of the field.
      [
        'type' => 'string_textfield',
        'settings' => [
          'size' => 60,
          'placeholder' => '',
        ],
      ],
      // The view displays the field should be added to and the field weight in
      // the view display.
      ['default' => 13, 'full' => 12, 'teaser' => 12],
      // The formatter settings of the field.
      ['type' => 'string', 'settings' => ['link_to_entity' => FALSE]]
    );

    // Assert the removed text field from the entity type defaults is not
    // created.
    $field_storage_storage = $entity_type_manager->getStorage('field_storage_config');
    $field_instance_storage = $entity_type_manager->getStorage('field_config');
    $field_storage = $field_storage_storage->load('node.field_entity_text_removed');
    static::assertNull($field_storage);
    $field_instance = $field_instance_storage->load('node.test_bundle.field_entity_text_removed');
    static::assertNull($field_instance);
  }

  /**
   * Asserts that a base field is correctly created and added to displays.
   *
   * @param string $field_name
   *   The field name.
   * @param array $base_field_override_settings
   *   The base field override settings.
   * @param array $form_displays
   *   The form displays to which the field should be added.
   * @param array $form_display_settings
   *   The form display settings.
   * @param array $view_displays
   *   The view displays to which the field should be added.
   * @param array $view_display_settings
   *   The view display settings.
   */
  protected function assertBaseField(string $field_name, array $base_field_override_settings, array $form_displays, array $form_display_settings, array $view_displays, array $view_display_settings): void {
    $entity_type_manager = $this->container->get('entity_type.manager');
    $entity_field_manager = $this->container->get('entity_field.manager');

    $form_display_storage = $entity_type_manager->getStorage('entity_form_display');
    $view_display_storage = $entity_type_manager->getStorage('entity_view_display');

    $field_definitions = $entity_field_manager->getFieldDefinitions('node', 'test_bundle');

    // Assert the base field title and description are correct.
    static::assertSame($field_definitions[$field_name]->getLabel(), $base_field_override_settings['label'], sprintf('The label base field override setting of the %s base field is not correct.', $field_name));
    static::assertSame($field_definitions[$field_name]->getDescription(), $base_field_override_settings['description'] ?? '', sprintf('The description base field override setting of the %s base field is not correct.', $field_name));

    // Assert the field is added to the correct form displays (including the
    // correct widget settings).
    foreach ($form_displays as $display_id => $weight) {
      $form_display_component = $form_display_storage->load('node.test_bundle.' . $display_id)->getComponent($field_name);
      static::assertSame($form_display_component['type'], $form_display_settings['type'], sprintf('The widget type of the %s base field is not correct.', $field_name));
      static::assertSame($form_display_component['weight'], $weight, sprintf('The weight of the %s base field in the %s form display is not correct.', $field_name, $display_id));
      static::assertSame($form_display_component['settings'], $form_display_settings['settings'], sprintf('The widget settings of the %s base field are not correct.', $field_name));
    }
    // Assert the field is not added to the other form displays.
    $other_form_displays = array_diff(['default', 'short'], array_keys($form_displays));
    foreach ($other_form_displays as $display_id) {
      static::assertNull($form_display_storage->load('node.test_bundle.' . $display_id)->getComponent($field_name), sprintf('The %s base field is added to the %s form display while it shouldn\'t be.', $field_name, $display_id));
    }

    // Assert the field is added to the correct form displays (including the
    // correct widget settings).
    foreach ($view_displays as $display_id => $weight) {
      $view_display_component = $view_display_storage->load('node.test_bundle.' . $display_id)->getComponent($field_name);
      static::assertSame($view_display_component['type'], $view_display_settings['type'], sprintf('The formatter type of the %s base field is not correct.', $field_name));
      static::assertSame($view_display_component['label'], $view_display_settings['label'] ?? 'hidden', sprintf('The formatter label of the %s base field is not correct.', $field_name));
      static::assertSame($view_display_component['weight'], $weight, sprintf('The weight of the %s base field in the %s view display is not correct.', $field_name, $display_id));
      static::assertSame($view_display_component['settings'], $view_display_settings['settings'], sprintf('The formatter settings of the %s base field are not correct.', $field_name));
    }
    // Assert the field is not added to the other view displays.
    $other_view_displays = array_diff(['default', 'full', 'teaser'], array_keys($view_displays));
    foreach ($other_view_displays as $display_id) {
      static::assertNull($view_display_storage->load('node.test_bundle.' . $display_id)->getComponent($field_name), sprintf('The %s base field is added to the %s view display while it shouldn\'t be.', $field_name, $display_id));
    }
  }

  /**
   * Asserts that a field is correctly created and added to displays.
   *
   * @param string $field_name
   *   The field name.
   * @param array $storage_settings
   *   The field storage settings.
   * @param array $instance_settings
   *   The field instance settings.
   * @param array $form_displays
   *   The form displays to which the field should be added.
   * @param array $form_display_settings
   *   The form display settings.
   * @param array $view_displays
   *   The view displays to which the field should be added.
   * @param array $view_display_settings
   *   The view display settings.
   */
  protected function assertCustomField(string $field_name, array $storage_settings, array $instance_settings, array $form_displays, array $form_display_settings, array $view_displays, array $view_display_settings): void {
    $entity_type_manager = $this->container->get('entity_type.manager');

    $field_storage_storage = $entity_type_manager->getStorage('field_storage_config');
    $field_instance_storage = $entity_type_manager->getStorage('field_config');
    $form_display_storage = $entity_type_manager->getStorage('entity_form_display');
    $view_display_storage = $entity_type_manager->getStorage('entity_view_display');

    $field_storage = $field_storage_storage->load('node.' . $field_name);
    static::assertInstanceOf(FieldStorageConfigInterface::class, $field_storage, sprintf('The storage for the %s custom field is not created.', $field_name));
    static::assertSame($field_storage->get('type'), $storage_settings['type'], sprintf('The storage type of the %s custom field is not correct.', $field_name));
    static::assertSame($field_storage->get('cardinality'), $storage_settings['cardinality'], sprintf('The cardinality of the %s custom field is not correct.', $field_name));
    static::assertSame($field_storage->get('settings'), $storage_settings['settings'], sprintf('The storage settings of the %s custom field are not correct.', $field_name));

    $field_instance = $field_instance_storage->load('node.test_bundle.' . $field_name);
    static::assertInstanceOf(FieldConfigInterface::class, $field_instance, sprintf('The instance for the %s custom field is not created.', $field_name));
    static::assertSame($field_instance->get('label'), $instance_settings['label'], sprintf('The label of the %s custom field is not correct.', $field_name));
    static::assertSame($field_instance->get('description'), $instance_settings['description'] ?? '', sprintf('The description of the %s custom field is not correct.', $field_name));
    static::assertSame($field_instance->get('required'), $instance_settings['required'] ?? FALSE, sprintf('The required status of the %s custom field is not correct.', $field_name));
    static::assertSame($field_instance->get('default_value'), $instance_settings['default_value'] ?? [], sprintf('The default value of the %s custom field is not correct.', $field_name));
    static::assertSame($field_instance->get('settings'), $instance_settings['settings'] ?? [], sprintf('The instance settings of the %s custom field are not correct.', $field_name));

    // Assert the field is added to the correct form displays (including the
    // correct widget settings).
    foreach ($form_displays as $display_id => $weight) {
      $form_display_component = $form_display_storage->load('node.test_bundle.' . $display_id)->getComponent($field_name);
      static::assertSame($form_display_component['type'], $form_display_settings['type'], sprintf('The widget type of the %s custom field is not correct.', $field_name));
      static::assertSame($form_display_component['weight'], $weight, sprintf('The weight of the %s custom field in the %s form display is not correct.', $field_name, $display_id));
      static::assertSame($form_display_component['settings'], $form_display_settings['settings'], sprintf('The widget settings of the %s custom field are not correct.', $field_name));
    }
    // Assert the field is not added to the other form displays.
    $other_form_displays = array_diff(['default', 'short'], array_keys($form_displays));
    foreach ($other_form_displays as $display_id) {
      static::assertNull($form_display_storage->load('node.test_bundle.' . $display_id)->getComponent($field_name), sprintf('The %s custom field is added to the %s form display while it shouldn\'t be.', $field_name, $display_id));
    }

    // Assert the field is added to the correct form displays (including the
    // correct widget settings).
    foreach ($view_displays as $display_id => $weight) {
      $view_display_component = $view_display_storage->load('node.test_bundle.' . $display_id)->getComponent($field_name);
      static::assertSame($view_display_component['type'], $view_display_settings['type'], sprintf('The formatter type of the %s custom field is not correct.', $field_name));
      static::assertSame($view_display_component['label'], $view_display_settings['label'] ?? 'hidden', sprintf('The formatter label of the %s custom field is not correct.', $field_name));
      static::assertSame($view_display_component['weight'], $weight, sprintf('The weight of the %s base field in the %s view display is not correct.', $field_name, $display_id));
      static::assertSame($view_display_component['settings'], $view_display_settings['settings'], sprintf('The formatter settings of the %s custom field are not correct.', $field_name));
    }
    // Assert the field is not added to the other view displays.
    $other_view_displays = array_diff(['default', 'full', 'teaser'], array_keys($view_displays));
    foreach ($other_view_displays as $display_id) {
      static::assertNull($view_display_storage->load('node.test_bundle.' . $display_id)->getComponent($field_name), sprintf('The %s custom field is added to the %s view display while it shouldn\'t be.', $field_name, $display_id));
    }
  }

}
