<?php

namespace Drupal\yaml_bundles\Commands;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\language\ConfigurableLanguageManagerInterface;
use Drupal\yaml_bundles\BundleCreator;
use Drupal\yaml_bundles\Helper\DisplayCreator;
use Drupal\yaml_bundles\Helper\FieldCreator;
use Drupal\yaml_bundles\Helper\SettingsCreator;
use Drupal\yaml_bundles\Plugin\EntityTypePluginManager;
use Drupal\yaml_bundles\Plugin\FieldTypePluginManager;
use Drush\Commands\DrushCommands;

/**
 * Drush commands to create entity bundles and fields.
 */
class CreateBundles extends DrushCommands {

  /**
   * The bundle creator.
   *
   * @var \Drupal\yaml_bundles\BundleCreator
   */
  protected BundleCreator $bundleCreator;

  /**
   * The bundle settings creator.
   *
   * @var \Drupal\yaml_bundles\Helper\SettingsCreator
   */
  protected SettingsCreator $settingsCreator;

  /**
   * The bundle field creator.
   *
   * @var \Drupal\yaml_bundles\Helper\FieldCreator
   */
  protected FieldCreator $fieldCreator;

  /**
   * The bundle display creator.
   *
   * @var \Drupal\yaml_bundles\Helper\DisplayCreator
   */
  protected DisplayCreator $displayCreator;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity type plugin manager.
   *
   * @var \Drupal\yaml_bundles\Plugin\EntityTypePluginManager
   */
  protected EntityTypePluginManager $entityTypePluginManager;

  /**
   * The language manager.
   *
   * @var \Drupal\language\ConfigurableLanguageManagerInterface
   */
  protected ConfigurableLanguageManagerInterface $languageManager;

  /**
   * The field type plugin manager.
   *
   * @var \Drupal\yaml_bundles\Plugin\FieldTypePluginManager
   */
  protected FieldTypePluginManager $fieldTypePluginManager;

  /**
   * Constructs a CreateBundles instance.
   *
   * @param \Drupal\yaml_bundles\BundleCreator $bundle_creator
   *   The bundle creator.
   * @param \Drupal\yaml_bundles\Helper\SettingsCreator $settings_creator
   *   The bundle settings creator.
   * @param \Drupal\yaml_bundles\Helper\FieldCreator $field_creator
   *   The bundle field creator.
   * @param \Drupal\yaml_bundles\Helper\DisplayCreator $display_creator
   *   The bundle display creator.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\language\ConfigurableLanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(BundleCreator $bundle_creator, SettingsCreator $settings_creator, FieldCreator $field_creator, DisplayCreator $display_creator, ModuleHandlerInterface $module_handler, EntityTypeManagerInterface $entity_type_manager, ConfigurableLanguageManagerInterface $language_manager) {
    parent::__construct();
    $this->bundleCreator = $bundle_creator;
    $this->settingsCreator = $settings_creator;
    $this->fieldCreator = $field_creator;
    $this->displayCreator = $display_creator;
    $this->moduleHandler = $module_handler;
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
  }

  /**
   * Creates custom entity bundles from YAML plugins.
   *
   * @usage drush yaml-bundles:create-bundles
   *   Creates custom entity bundles.
   *
   * @command yaml-bundles:create-bundles
   *
   * @aliases ybcbs
   */
  public function createBundles(): void {
    $this->bundleCreator->createBundles();
  }

  /**
   * Creates a custom entity bundle.
   *
   * @usage drush yaml-bundles:create-bundle
   *   Creates a custom entity bundle.
   *
   * @command yaml-bundles:create-bundle
   *
   * @aliases ybcb
   */
  public function createBundle(): void {
    $entity_type_id = $this->io()->ask('For which entity type do you want to create a bundle?');
    if (!$entity_type_id) {
      return;
    }

    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
    $bundle_entity_type_id = $entity_type->getBundleEntityType();

    // Check if the entity type allows bundles.
    if (!$bundle_entity_type_id) {
      $this->io()->error('The entity type ' . $entity_type_id . ' does not allow bundles.');
      return;
    }

    $bundle_id = $this->io()->ask('What is the technical name of the bundle?');
    if (!$bundle_id) {
      return;
    }

    // Create the settings array.
    $settings = [
      'label' => $this->io()->ask('What is the label of the bundle in ' . $this->languageManager->getDefaultLanguage()->getName() . '?', ucfirst(str_replace('_', ' ', $bundle_id))),
      'description' => $this->io()->ask('What is the description of the bundle in ' . $this->languageManager->getDefaultLanguage()->getName() . ' (optional)?'),
      'langcode' => $this->languageManager->getDefaultLanguage()->getId(),
    ];

    // Check if the bundle label is not empty.
    if (empty($settings['label'])) {
      $this->io()->error('The bundle label cannot be empty.');
      return;
    }

    // Add the bundle label and description translations.
    foreach ($this->languageManager->getLanguages() as $language) {
      if ($language->getId() === $this->languageManager->getDefaultLanguage()->getId()) {
        continue;
      }
      $settings['translations'][$language->getId()]['label'] = $this->io()->ask('What is the label of the bundle in ' . $language->getName() . '?', $settings['label']);
      $settings['translations'][$language->getId()]['description'] = $this->io()->ask('What is the description of the bundle in ' . $language->getName() . '?', $settings['description']);
    }

    // Ask for the path alias settings for the bundle.
    if ($this->moduleHandler->moduleExists('pathauto')) {
      $settings['path'] = $this->io()->ask('What is the pathauto pattern for this bundle?', $bundle_id . '/[node:title]');
    }

    // Create the bundle.
    $this->bundleCreator->createBundle($entity_type_id, $bundle_id, $settings);
  }

  /**
   * Creates a custom entity bundle field.
   *
   * @usage drush yaml-bundles:create-field
   *   Creates a custom entity bundle.
   *
   * @command yaml-bundles:create-field
   *
   * @aliases ybcf
   */
  public function createField(): void {
    $entity_type_id = $this->io()->ask('For which entity type do you want to create a field?');
    if (!$entity_type_id) {
      return;
    }

    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
    $bundle_entity_type_id = $entity_type->getBundleEntityType();

    // Check if the entity type allows bundles.
    if (!$bundle_entity_type_id) {
      $this->io()->error('The entity type ' . $entity_type_id . ' does not allow bundles.');
      return;
    }

    $bundle_id = $this->io()->ask('What is the technical name of the bundle?');
    if (!$bundle_id) {
      $this->io()->error('The bundle name cannot be empty.');
      return;
    }

    $bundle = $this->entityTypeManager->getStorage($bundle_entity_type_id)->load($bundle_id);
    if (!$bundle) {
      $this->io()->error('The bundle ' . $bundle_id . ' does not exist.');
      return;
    }

    $field_name = $this->io()->ask('What is the technical name of the field (usually prefixed with "field_")?');
    if (!$field_name) {
      $this->io()->error('The field name cannot be empty.');
      return;
    }

    $settings = $this->entityTypeManager->getStorage($bundle_entity_type_id)->load($bundle_id)->toArray();

    // Create the field settings array.
    $field_types = [
      'string',
      'integer',
      'text_long',
      'media',
      'link',
      'boolean',
      'date',
      'list_string',
    ];
    $settings['fields'][$field_name] = [
      'type' => $field_types[$this->io()->choice('What is the field type?', $field_types)],
      'label' => $this->io()->ask('What is the label of the field in ' . $this->languageManager->getDefaultLanguage()->getName() . '?', ucfirst(str_replace('_', ' ', str_replace('field_', '', $field_name)))),
      'description' => $this->io()->ask('What is the description of the field in ' . $this->languageManager->getDefaultLanguage()->getName() . ' (optional)?'),
    ];

    // Check if the field label is not empty.
    if (empty($settings['fields'][$field_name]['label'])) {
      $this->io()->error('The field label cannot be empty.');
      return;
    }

    // Add the field label and description translations.
    foreach ($this->languageManager->getLanguages() as $language) {
      if ($language->getId() === $this->languageManager->getDefaultLanguage()->getId()) {
        continue;
      }
      $settings['translations'][$language->getId()]['fields'][$field_name]['label'] = $this->io()->ask('What is the label of the field in ' . $language->getName() . '?', $settings['fields'][$field_name]['label']);
      $settings['translations'][$language->getId()]['fields'][$field_name]['description'] = $this->io()->ask('What is the description of the field in ' . $language->getName() . '?', $settings['fields'][$field_name]['description']);
    }

    // Ask for additional field settings.
    $settings['fields'][$field_name]['required'] = $this->io()->confirm('Is the field required?');
    $settings['fields'][$field_name]['translatable'] = $this->io()->confirm('Is the field translatable?');
    $settings['fields'][$field_name]['cardinality'] = $this->io()->ask('What is the cardinality of the field (set to -1 to allow unlimited values)?', '1');

    // Ask if the field should be added to the search API index if available.
    if ($this->moduleHandler->moduleExists('search_api')) {
      $settings['fields'][$field_name]['search'] = $this->io()->confirm('Do you want to add the field to the search API index?');

      $text_types = ['string', 'text_long'];
      if (in_array($settings['fields'][$field_name]['type'], $text_types, TRUE)) {
        $settings['fields'][$field_name]['search_boost'] = $this->io()->ask('How much do you want to boost a match in the field when searching (set a value between 1 and 20 for optimal results)?', '1');
      }
    }

    // Ask for the media types in media fields.
    if ('media' === $settings['fields'][$field_name]['type']) {
      $media_types = $this->entityTypeManager->getStorage('media_type')->loadMultiple();
      foreach ($media_types as $media_type) {
        $allow = $this->io()->confirm(sprintf('Do you want to allow the media type %s in the field?', $media_type->label()));
        if (!$allow) {
          continue;
        }
        $settings['fields'][$field_name]['field_settings']['handler_settings']['target_bundles'][] = $media_type->id();
        $settings['fields'][$field_name]['widget_settings']['media_types'][] = $media_type->id();
      }
    }

    // Merge the available default settings from the entity type, bundle and
    // field plugins.
    $settings = $this->settingsCreator->finalizeSettings($entity_type_id, $settings);

    // Create the field.
    $this->fieldCreator->createField($entity_type_id, $bundle_id, $field_name, $settings);

    // Add the field to the form displays.
    $form_display_ids = $this->entityTypeManager->getStorage('entity_form_display')->getQuery()
      ->condition('targetEntityType', $entity_type_id)
      ->condition('bundle', $bundle_id)
      ->execute();
    foreach ($form_display_ids as $form_display_id) {
      $form_display = $this->entityTypeManager->getStorage('entity_form_display')->load($form_display_id);
      if ($form_display instanceof EntityFormDisplay && $this->io()->confirm('Do you want to add the field to the ' . $form_display_id . ' form display?')) {
        $this->displayCreator->addFieldToFormDisplay($entity_type_id, $form_display, $field_name, $settings);
        $form_display->save();
      }
    }

    // Add the field to the default view display.
    $view_display_ids = $this->entityTypeManager->getStorage('entity_view_display')->getQuery()
      ->condition('targetEntityType', $entity_type_id)
      ->condition('bundle', $bundle_id)
      ->execute();
    foreach ($view_display_ids as $view_display_id) {
      $view_display = $this->entityTypeManager->getStorage('entity_view_display')->load($view_display_id);
      if ($view_display instanceof EntityViewDisplay && $this->io()->confirm('Do you want to add the field to the ' . $view_display_id . ' view display?')) {
        $this->displayCreator->addFieldToViewDisplay($entity_type_id, $view_display, $field_name, $settings);
        $view_display->save();
      }
    }
  }

}
