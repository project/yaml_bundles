<?php

namespace Drupal\yaml_bundles\Helper;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\filter\Entity\FilterFormat;
use Drupal\yaml_bundles\Plugin\EntityTypePluginManager;
use Drupal\yaml_bundles\Plugin\FieldTypePluginManager;
use Drupal\yaml_bundles\Plugin\GroupTypePluginManager;

/**
 * Helper service to merge and validate bundle settings.
 */
class SettingsCreator {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The entity type plugin manager.
   *
   * @var \Drupal\yaml_bundles\Plugin\EntityTypePluginManager
   */
  protected EntityTypePluginManager $entityTypePluginManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * The field type plugin manager.
   *
   * @var \Drupal\yaml_bundles\Plugin\FieldTypePluginManager
   */
  protected FieldTypePluginManager $fieldTypePluginManager;

  /**
   * The group type plugin manager.
   *
   * @var \Drupal\yaml_bundles\Plugin\GroupTypePluginManager
   */
  protected GroupTypePluginManager $groupTypePluginManager;

  /**
   * Constructs a SettingsCreator instance.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\yaml_bundles\Plugin\EntityTypePluginManager $entity_type_plugin_manager
   *   The entity type plugin manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\yaml_bundles\Plugin\FieldTypePluginManager $field_type_plugin_manager
   *   The field type plugin manager.
   * @param \Drupal\yaml_bundles\Plugin\GroupTypePluginManager $group_type_plugin_manager
   *   The group type plugin manager.
   */
  public function __construct(ModuleHandlerInterface $module_handler, EntityTypePluginManager $entity_type_plugin_manager, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, FieldTypePluginManager $field_type_plugin_manager, GroupTypePluginManager $group_type_plugin_manager) {
    $this->moduleHandler = $module_handler;
    $this->entityTypePluginManager = $entity_type_plugin_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->fieldTypePluginManager = $field_type_plugin_manager;
    $this->groupTypePluginManager = $group_type_plugin_manager;
  }

  /**
   * Finalizes the available configuration for an entity bundle.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param array $settings
   *   The bundle settings.
   *
   * @return array
   *   The finalized bundle settings.
   *
   * @throws \InvalidArgumentException
   */
  public function finalizeSettings(string $entity_type_id, array $settings): array {
    // Merge the default entity type settings.
    if ($this->entityTypePluginManager->hasDefinition($entity_type_id)) {
      // Unset the ID, label and description as they might conflict with the
      // bundle settings.
      $definition_settings = $this->entityTypePluginManager->getDefinition($entity_type_id);
      unset($definition_settings['id'], $definition_settings['label'], $definition_settings['description']);
      $settings = NestedArray::mergeDeep($definition_settings, $settings);
    }

    // Finalize the field settings.
    $settings = $this->finalizeFieldSettings($entity_type_id, $settings);

    // Finalize the group settings when the field_group module is enabled. If
    // not, unset all groups.
    if ($this->moduleHandler->moduleExists('field_group')) {
      $settings = $this->finalizeGroupSettings($entity_type_id, $settings);
    }
    else {
      $settings['groups'] = [];
    }

    // Add the layout builder field when layout builder is enabled.
    $enable_layout_builder = $settings['layout_builder'] ?? FALSE;
    if ($enable_layout_builder && $this->moduleHandler->moduleExists('layout_builder')) {
      $settings['fields']['layout_builder__layout'] = [
        'label' => 'Layout',
        'field_type' => 'layout_section',
      ];
    }

    // Check if the required bundle / field settings are present. This will
    // throw an exception if the required settings are not present.
    $this->validateBundleSettings($entity_type_id, $settings);

    return $settings;
  }

  /**
   * Finalizes the available field configuration.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param array $settings
   *   The bundle settings.
   *
   * @return array
   *   The finalized bundle settings with updated field configuration.
   *
   * @throws \InvalidArgumentException
   */
  public function finalizeFieldSettings(string $entity_type_id, array $settings): array {
    // Make sure the fields key is set.
    $settings['fields'] = $settings['fields'] ?? [];

    // Filter out the removed fields.
    $settings['fields'] = array_filter($settings['fields']);

    // Sort the fields by weight.
    uasort($settings['fields'], function ($a, $b) {
      return ($a['weight'] ?? 0) <=> ($b['weight'] ?? 0);
    });

    // Merge the default field type settings. We fetch the base fields so we can
    // set the correct field type for base fields.
    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $base_fields */
    $base_fields = $this->entityFieldManager->getBaseFieldDefinitions($entity_type_id);
    foreach (array_keys($settings['fields']) as $field_name) {
      if (isset($base_fields[$field_name])) {
        $settings['fields'][$field_name]['type'] = $base_fields[$field_name]->getType();
      }

      // Merge the field settings with the default field type settings.
      if ($this->fieldTypePluginManager->hasDefinition($settings['fields'][$field_name]['type'])) {
        $settings['fields'][$field_name] = NestedArray::mergeDeep($this->fieldTypePluginManager->getDefinition($settings['fields'][$field_name]['type']), $settings['fields'][$field_name]);
      }

      // Set the field type to the default field type if it is not set.
      if (!isset($settings['fields'][$field_name]['field_type'])) {
        $settings['fields'][$field_name]['field_type'] = $settings['fields'][$field_name]['type'];
      }
    }

    // Update the shorthand field settings.
    foreach ($settings['fields'] as $field_name => $field_settings) {
      // Merge the options as allowed values.
      if (isset($field_settings['options'])) {
        foreach ($field_settings['options'] as $value => $label) {
          $settings['fields'][$field_name]['storage_settings']['allowed_values'][$value] = $label;
        }
      }

      // Remove the allowed values when an allowed values callback is set.
      if (isset($field_settings['storage_settings']['allowed_values_function'])) {
        $settings['fields'][$field_name]['storage_settings']['allowed_values'] = [];
      }

      // Merge the min value for number fields.
      if (isset($field_settings['min'])) {
        $settings['fields'][$field_name]['field_settings']['min'] = $field_settings['min'];
      }

      // Merge the min value for number fields.
      if (isset($field_settings['max'])) {
        $settings['fields'][$field_name]['field_settings']['max'] = $field_settings['max'];
      }

      // Merge the default value for simple fields.
      if (isset($field_settings['default'])) {
        unset($settings['fields'][$field_name]['field_default_value']);
        $settings['fields'][$field_name]['field_default_value'][0]['value'] = $field_settings['default'];
      }

      // Merge the reference types for entity references.
      if (isset($field_settings['reference_types'])) {
        $settings['fields'][$field_name]['field_settings']['handler_settings']['target_bundles'] = $field_settings['reference_types'];
      }
    }

    // Update the shorthand field settings for translations.
    foreach ($settings['translations'] as $langcode => $translation_settings) {
      foreach ($translation_settings['fields'] ?? [] as $field_name => $field_settings) {
        // Merge the options as allowed values.
        if (isset($field_settings['options']) && !empty($settings['fields'][$field_name]['storage_settings']['allowed_values'])) {
          foreach ($field_settings['options'] as $label) {
            $settings['translations'][$langcode]['fields'][$field_name]['storage_settings']['allowed_values'][]['label'] = $label;
          }
        }
        elseif (isset($field_settings['options'])) {
          $settings['translations'][$langcode]['fields'][$field_name]['storage_settings']['allowed_values'] = [];
        }
      }
    }

    return $settings;
  }

  /**
   * Finalizes the available group configuration.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param array $settings
   *   The bundle settings.
   *
   * @return array
   *   The finalized bundle settings with updated group configuration.
   *
   * @throws \InvalidArgumentException
   */
  public function finalizeGroupSettings(string $entity_type_id, array $settings): array {
    // Make sure the groups key is set.
    $settings['groups'] = $settings['groups'] ?? [];

    // Filter out the removed groups.
    $settings['groups'] = array_filter($settings['groups']);

    // Sort the groups by weight.
    uasort($settings['groups'], function ($a, $b) {
      return ($a['weight'] ?? 0) <=> ($b['weight'] ?? 0);
    });

    // Merge the default group type settings.
    foreach (array_keys($settings['groups']) as $group_name) {
      // Merge the group settings with the default group type settings.
      if ($this->groupTypePluginManager->hasDefinition($settings['groups'][$group_name]['type'])) {
        $settings['groups'][$group_name] = NestedArray::mergeDeep($this->groupTypePluginManager->getDefinition($settings['groups'][$group_name]['type']), $settings['groups'][$group_name]);
      }

      // Set the group type to the default group type if it is not set.
      if (!isset($settings['groups'][$group_name]['group_type'])) {
        $settings['groups'][$group_name]['group_type'] = $settings['groups'][$group_name]['type'];
      }
    }

    // Update the shorthand group settings.
    foreach ($settings['groups'] as $group_name => $group_settings) {
      // When the group has a parent, make sure the group is added to the field
      // list of the parent group.
      if (isset($group_settings['parent'], $settings['groups'][$group_settings['parent']]) && !in_array($group_name, $settings['groups'][$group_settings['parent']]['fields'] ?? [], TRUE)) {
        $settings['groups'][$group_settings['parent']]['fields'][] = $group_name;
      }

      // Update the settings for "markup" group types.
      if ('markup' === $group_settings['group_type']) {
        if (isset($group_settings['description'])) {
          $settings['groups'][$group_name]['settings']['markup']['value'] = $group_settings['description'];
          unset($group_settings['description'], $settings['groups'][$group_name]['description']);
        }

        // The format setting will be used as the markup format.
        if (isset($group_settings['format'])) {
          $settings['groups'][$group_name]['settings']['markup']['format'] = $group_settings['format'];
          unset($group_settings['format'], $settings['groups'][$group_name]['format']);
        }
        elseif ($this->moduleHandler->moduleExists('filter')) {
          $filter_formats = $this->entityTypeManager->getStorage('filter_format')->loadMultiple();
          uasort($filter_formats, [FilterFormat::class, 'sort']);
          $settings['groups'][$group_name]['settings']['markup']['format'] = array_key_first($filter_formats);
        }
      }

      // If the group contains a description, move it to the settings array
      // since that is where the field group module stores it.
      if (isset($group_settings['description'])) {
        $settings['groups'][$group_name]['settings']['description'] = $group_settings['description'];
        unset($settings['groups'][$group_name]['description']);
      }

      // Add the group to the content region by default.
      if (!isset($settings['groups'][$group_name]['region'])) {
        $settings['groups'][$group_name]['region'] = 'content';
      }
    }

    // Update the shorthand group settings for translations.
    foreach ($settings['translations'] as $langcode => $translation_settings) {
      foreach ($translation_settings['groups'] ?? [] as $group_name => $group_settings) {
        // Update the translation settings for "markup" group types.
        if ('markup' === $settings['groups'][$group_name]['group_type']) {
          if (isset($group_settings['description'])) {
            $settings['translations'][$langcode]['groups'][$group_name]['settings']['markup']['value'] = $group_settings['description'];
            unset($group_settings['description'], $settings['translations'][$langcode]['groups'][$group_name]['description']);
          }
        }

        // If the translation contains a description, move it to the settings
        // array since that is where the field group module stores it.
        if (isset($group_settings['description'])) {
          $settings['translations'][$langcode]['groups'][$group_name]['settings']['description'] = $group_settings['description'];
          unset($group_settings['description'], $settings['translations'][$langcode]['groups'][$group_name]['description']);
        }
      }
    }

    return $settings;
  }

  /**
   * Validates if the required bundle / field settings are present.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param array $settings
   *   The bundle settings.
   *
   * @return bool
   *   Whether the bundle settings are valid or not.
   *
   * @throws \InvalidArgumentException
   */
  protected function validateBundleSettings(string $entity_type_id, array $settings): bool {
    $this->hasRequiredSettingsRecursive($entity_type_id, EntityTypePluginManager::REQUIRED_KEYS[$entity_type_id] ?? [], $settings);
    $base_fields = $this->entityFieldManager->getBaseFieldDefinitions($entity_type_id);
    foreach ($settings['fields'] as $field_name => $field_settings) {
      $is_base_field = isset($base_fields[$field_name]);

      // Check if the field at least has a type when it is not a base field.
      if (!$is_base_field && !isset($field_settings['field_type'])) {
        throw new \InvalidArgumentException(sprintf('The field %s for entity type %s does not have a type.', $field_name, $entity_type_id));
      }

      // Check if the required field settings are present for the field type.
      $this->hasRequiredSettingsRecursive($entity_type_id, FieldTypePluginManager::REQUIRED_KEYS[$field_settings['field_type']] ?? [], $field_settings);
    }
    return TRUE;
  }

  /**
   * Checks recursively if a required field setting is available.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param array $required_keys
   *   The required keys.
   * @param array $settings
   *   The settings.
   * @param array $parents
   *   The parents.
   *
   * @return bool
   *   Whether the required settings are available or not.
   *
   * @throws \InvalidArgumentException
   */
  protected function hasRequiredSettingsRecursive(string $entity_type_id, array $required_keys, array $settings, array $parents = []): bool {
    foreach ($required_keys as $key => $value) {
      // Call the method recursively if we have another level of required keys.
      if (is_array($value)) {
        $parents[] = $key;
        return $this->hasRequiredSettingsRecursive($entity_type_id, $value, $settings[$key], $parents);
      }

      // If we reached the deepest level of required keys, check if the setting
      // is available. It is possible to require only one setting from a list of
      // settings, in that case the settings are split by a pipe (for example
      // the allowed_values|allowed_values_function keys in a list field).
      if (!array_intersect(explode('|', $value), array_keys($settings))) {
        throw new \InvalidArgumentException(sprintf('The required key %s is missing for the entity type %s.', implode(' > ', $parents), $entity_type_id));
      }
    }
    return TRUE;
  }

}
