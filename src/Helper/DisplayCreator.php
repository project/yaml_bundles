<?php

namespace Drupal\yaml_bundles\Helper;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\Entity\EntityFormMode;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Entity\Entity\EntityViewMode;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Helper service to create displays for entity bundles.
 */
class DisplayCreator {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * The current display component weight.
   *
   * @var int
   */
  protected int $componentWeight = 0;

  /**
   * Constructs a DisplayCreator instance.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(ModuleHandlerInterface $module_handler, LanguageManagerInterface $language_manager, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager) {
    $this->moduleHandler = $module_handler;
    $this->languageManager = $language_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * Creates the form displays for the bundle.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $bundle_id
   *   The bundle ID.
   * @param array $settings
   *   The bundle settings.
   */
  public function createFormDisplays(string $entity_type_id, string $bundle_id, array $settings): void {
    $field_definitions = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle_id);
    $display_ids = array_unique(array_merge($settings['form_displays'] ?? [], ['default']));
    foreach ($display_ids as $display_id) {
      // Create the form mode entity if it doesn't exist. The default form mode
      // always exists, so we don't have to create it.
      if ('default' !== $display_id) {
        $form_mode = $this->entityTypeManager->getStorage('entity_form_mode')->load(implode('.', [
          $entity_type_id,
          $display_id,
        ]));
        if (!$form_mode instanceof EntityFormMode) {
          $form_mode = $this->entityTypeManager->getStorage('entity_form_mode')->create([
            'id' => implode('.', [
              $entity_type_id,
              $display_id,
            ]),
            'label' => ucfirst($display_id),
            'targetEntityType' => $entity_type_id,
            'cache' => TRUE,
          ]);
          $form_mode->save();
        }
      }

      // Load the existing form display entity or create a new one.
      $form_display = $this->entityTypeManager->getStorage('entity_form_display')->load(implode('.', [
        $entity_type_id,
        $bundle_id,
        $display_id,
      ]));
      if (!$form_display instanceof EntityFormDisplay) {
        /** @var \Drupal\Core\Entity\Entity\EntityFormDisplay $form_display */
        $form_display = $this->entityTypeManager->getStorage('entity_form_display')
          ->create([
            'targetEntityType' => $entity_type_id,
            'bundle' => $bundle_id,
            'mode' => $display_id,
            'status' => TRUE,
          ]);
      }
      $form_display->set('langcode', $settings['langcode'] ?? $this->languageManager->getDefaultLanguage()->getId());

      // Remove all existing components.
      foreach (array_keys($form_display->getComponents()) as $name) {
        $form_display->removeComponent($name);
      }

      // Add fields to the form display.
      $this->componentWeight = 0;
      foreach ($settings['fields'] as $field_name => $field_settings) {
        // Check if the field should be added to the form display.
        if (!in_array($display_id, $field_settings['form_displays'] ?? [], TRUE) || !$field_definitions[$field_name]->isDisplayConfigurable('form')) {
          continue;
        }
        $this->addFieldToFormDisplay($entity_type_id, $form_display, $field_name, $settings);
      }

      // Add groups to the form display if the field group module is enabled.
      if ($this->moduleHandler->moduleExists('field_group')) {
        // Remove existing groups so they can be re-added.
        $groups = $form_display->getThirdPartySettings('field_group');
        foreach (array_keys($groups) as $group_name) {
          $form_display->unsetThirdPartySetting('field_group', $group_name);
        }

        // Add groups to the form display.
        $this->componentWeight = 0;
        foreach ($settings['groups'] as $group_name => $group_settings) {
          // Check if the group should be added to the form display.
          if (!in_array($display_id, $group_settings['form_displays'] ?? [], TRUE)) {
            continue;
          }
          $this->addGroupToFormDisplay($form_display, $group_name, $settings);
        }

        // Fix the weights for all fields/subgroups of the groups. We do this
        // after adding all groups to the form display, because we need to know
        // all groups have been created.
        foreach ($settings['groups'] as $group_settings) {
          // Update the weight of the fields in the group.
          $weight = 0;
          foreach ($group_settings['fields'] ?? [] as $field_name) {
            // Check if we have a component, in that case we update the weight.
            $component = $form_display->getComponent($field_name);
            if ($component) {
              $component['weight'] = $weight++;
              $form_display->setComponent($field_name, $component);
              continue;
            }

            // If we don't have a component, we are probably working with a
            // group. In that case we update the weight of the group.
            $group = $form_display->getThirdPartySetting('field_group', $field_name);
            if ($group) {
              $group['weight'] = $weight++;
              $form_display->setThirdPartySetting('field_group', $field_name, $group);
            }
          }
        }
      }

      // Save the form display.
      $form_display->save();

      // Translate the form display.
      if ($this->moduleHandler->moduleExists('language')) {
        $this->translateFormDisplay($entity_type_id, $bundle_id, $display_id, $settings);
      }
    }

    // Delete the form displays that are not configured.
    $form_displays = $this->entityTypeManager->getStorage('entity_form_display')->loadByProperties([
      'targetEntityType' => $entity_type_id,
      'bundle' => $bundle_id,
    ]);
    foreach ($form_displays as $form_display) {
      if (!in_array($form_display->getMode(), $display_ids, TRUE)) {
        $form_display->delete();
      }
    }
  }

  /**
   * Adds a field to a form display.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param \Drupal\Core\Entity\Entity\EntityFormDisplay $form_display
   *   The form display.
   * @param string $field_name
   *   The field machine name.
   * @param array $settings
   *   The bundle settings.
   */
  public function addFieldToFormDisplay(string $entity_type_id, EntityFormDisplay $form_display, string $field_name, array $settings): void {
    $field_settings = $settings['fields'][$field_name];

    // Validate if the field has a valid widget type.
    if (!isset($field_settings['widget_type'])) {
      throw new \InvalidArgumentException(sprintf('The widget type key for field %s in entity type %s is not configured.', $field_name, $entity_type_id));
    }

    // Get the component third party settings. We remove the settings for
    // disabled modules.
    if (isset($field_settings['widget_third_party_settings'])) {
      foreach ($field_settings['widget_third_party_settings'] as $module => $module_settings) {
        if (!$this->moduleHandler->moduleExists($module)) {
          unset($field_settings['widget_third_party_settings'][$module]);
          continue;
        }

        // Set a maxlength setting and text when configured.
        if ('maxlength' === $module && isset($field_settings['maxlength'])) {
          $field_settings['widget_third_party_settings'][$module]['maxlength_js'] = $field_settings['maxlength'];
        }
        if ('maxlength' === $module && isset($field_settings['maxlength_label'])) {
          $field_settings['widget_third_party_settings'][$module]['maxlength_js_label'] = $field_settings['maxlength_label'];
        }
        if ('maxlength' === $module && isset($field_settings['maxlength_enforce'])) {
          $field_settings['widget_third_party_settings'][$module]['maxlength_js_enforce'] = $field_settings['maxlength_enforce'];
        }
      }
    }

    // Add the field to the form display.
    $form_display->setComponent($field_name, [
      'type' => $field_settings['widget_type'],
      'settings' => $field_settings['widget_settings'] ?? [],
      'third_party_settings' => $field_settings['widget_third_party_settings'] ?? [],
      'weight' => $this->componentWeight++,
    ]);
  }

  /**
   * Adds a group to a form display.
   *
   * @param \Drupal\Core\Entity\Entity\EntityFormDisplay $form_display
   *   The form display.
   * @param string $group_name
   *   The group machine name.
   * @param array $settings
   *   The bundle settings.
   */
  public function addGroupToFormDisplay(EntityFormDisplay $form_display, string $group_name, array $settings): void {
    $group_settings = $settings['groups'][$group_name];

    $existing_group = $form_display->getThirdPartySetting('field_group', $group_name);
    $form_display->setThirdPartySetting('field_group', $group_name, [
      'label' => $group_settings['label'] ?? '',
      'parent_name' => $group_settings['parent'] ?? '',
      'region' => $group_settings['region'] ?? 'content',
      'weight' => $group_settings['weight'] ?? $existing_group['weight'] ?? $this->componentWeight++,
      'format_type' => $group_settings['group_type'],
      'format_settings' => $group_settings['settings'] ?? [],
      'children' => $group_settings['fields'] ?? [],
    ]);
  }

  /**
   * Translates the form display for the bundle.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $bundle_id
   *   The bundle ID.
   * @param string $display_id
   *   The form display ID.
   * @param array $settings
   *   The bundle settings.
   */
  protected function translateFormDisplay(string $entity_type_id, string $bundle_id, string $display_id, array $settings): void {
    /** @var \Drupal\language\ConfigurableLanguageManagerInterface $this->languageManager */
    $field_definitions = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle_id);
    foreach ($this->languageManager->getLanguages() as $language) {
      // Skip the default language.
      if ($language->getId() === $this->languageManager->getDefaultLanguage()->getId()) {
        continue;
      }

      // Translate the field settings in the form display.
      /** @var \Drupal\Core\Config\Config $config_translation */
      $config_translation = $this->languageManager->getLanguageConfigOverride($language->getId(), implode('.', [
        'core',
        'entity_form_display',
        $entity_type_id,
        $bundle_id,
        $display_id,
      ]));

      $translated_field_settings = [];
      foreach ($settings['fields'] as $field_name => $field_settings) {
        // Check if the field should be added to the form display.
        if (!in_array($display_id, $field_settings['form_displays'] ?? [], TRUE) || !$field_definitions[$field_name]->isDisplayConfigurable('form')) {
          continue;
        }

        // Check if the field is translated.
        if (!isset($settings['translations'][$language->getId()]['fields'][$field_name])) {
          continue;
        }

        $language_settings = $settings['translations'][$language->getId()]['fields'][$field_name];

        // Check if the field has translated widget settings.
        if (isset($language_settings['widget_settings'])) {
          $translated_field_settings[$field_name]['settings'] = $language_settings['widget_settings'];
        }

        // Get the widget third party settings. We remove the settings for
        // disabled modules.
        if (isset($language_settings['widget_third_party_settings'])) {
          foreach ($language_settings['widget_third_party_settings'] as $module => $module_settings) {
            if (!$this->moduleHandler->moduleExists($module)) {
              unset($language_settings['widget_third_party_settings'][$module]);
            }
          }
        }

        // Set the maxlength text when configured.
        if (isset($language_settings['maxlength_label']) && $this->moduleHandler->moduleExists('maxlength')) {
          $language_settings['widget_third_party_settings']['maxlength']['maxlength_js_label'] = $language_settings['maxlength_label'];
        }

        // Check if the field has translated widget third party settings.
        if (isset($language_settings['widget_third_party_settings'])) {
          $translated_field_settings[$field_name]['third_party_settings'] = $language_settings['widget_third_party_settings'];
        }
      }

      // Set the translated field settings.
      if (!empty($translated_field_settings)) {
        $config_translation->set('content', $translated_field_settings);
      }
      else {
        $config_translation->clear('content');
      }

      $translated_group_settings = [];
      foreach ($settings['groups'] as $group_name => $group_settings) {
        // Check if the group should be added to the form display.
        if (!in_array($display_id, $group_settings['form_displays'] ?? [], TRUE)) {
          continue;
        }

        // Check if the group is translated.
        if (!isset($settings['translations'][$language->getId()]['groups'][$group_name])) {
          continue;
        }

        $language_settings = $settings['translations'][$language->getId()]['groups'][$group_name];

        $translated_group_settings[$group_name] = array_filter([
          'label' => $language_settings['label'] ?? '',
          'format_settings' => array_filter($language_settings['settings'] ?? []),
        ]);
      }

      // Set the translated group settings.
      if (!empty($translated_group_settings)) {
        $config_translation->set('third_party_settings', [
          'field_group' => $translated_group_settings,
        ]);
      }
      else {
        $config_translation->clear('third_party_settings');
      }

      // Save the config translation when not empty.
      if ($config_translation->get()) {
        $config_translation->save();
      }
      else {
        $config_translation->delete();
      }
    }
  }

  /**
   * Creates the view displays for the bundle.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $bundle_id
   *   The bundle ID.
   * @param array $settings
   *   The bundle settings.
   */
  public function createViewDisplays(string $entity_type_id, string $bundle_id, array $settings): void {
    $field_definitions = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle_id);
    $display_ids = array_unique(array_merge($settings['view_displays'] ?? [], ['default']));
    foreach ($display_ids as $display_id) {
      // Create the view mode entity if it doesn't exist. The default view mode
      // always exists, so we don't have to create it.
      if ('default' !== $display_id) {
        $view_mode = $this->entityTypeManager->getStorage('entity_view_mode')->load(implode('.', [
          $entity_type_id,
          $display_id,
        ]));
        if (!$view_mode instanceof EntityViewMode) {
          $view_mode = $this->entityTypeManager->getStorage('entity_view_mode')->create([
            'id' => implode('.', [
              $entity_type_id,
              $display_id,
            ]),
            'label' => ucfirst($display_id),
            'targetEntityType' => $entity_type_id,
            'cache' => TRUE,
          ]);
          $view_mode->save();
        }
      }

      // Load the existing view display entity or create a new one.
      $view_display = $this->entityTypeManager->getStorage('entity_view_display')->load(implode('.', [
        $entity_type_id,
        $bundle_id,
        $display_id,
      ]));
      if (!$view_display instanceof EntityViewDisplay) {
        /** @var \Drupal\Core\Entity\Entity\EntityViewDisplay $view_display */
        $view_display = $this->entityTypeManager->getStorage('entity_view_display')
          ->create([
            'targetEntityType' => $entity_type_id,
            'bundle' => $bundle_id,
            'mode' => $display_id,
            'status' => TRUE,
          ]);
      }
      $view_display->set('langcode', $settings['langcode'] ?? $this->languageManager->getDefaultLanguage()->getId());

      // Enable layout builder for the default display when layout builder is
      // enabled.
      $enable_layout_builder = $settings['layout_builder'] ?? FALSE;
      if ($enable_layout_builder && 'default' === $display_id && $this->moduleHandler->moduleExists('layout_builder')) {
        $view_display->setThirdPartySetting('layout_builder', 'enabled', TRUE);
        $view_display->setThirdPartySetting('layout_builder', 'allow_custom', TRUE);
      }

      // Remove all existing components.
      foreach (array_keys($view_display->getComponents()) as $name) {
        $view_display->removeComponent($name);
      }

      // Add fields to the form display.
      $this->componentWeight = 0;
      foreach ($settings['fields'] as $field_name => $field_settings) {
        // Check if the field should be added to the view display.
        if (!in_array($display_id, $field_settings['view_displays'] ?? [], TRUE) || !$field_definitions[$field_name]->isDisplayConfigurable('view')) {
          continue;
        }
        $this->addFieldToViewDisplay($entity_type_id, $view_display, $field_name, $settings);
      }

      // Add groups to the view display if the field group module is enabled.
      if ($this->moduleHandler->moduleExists('field_group')) {
        // Remove existing groups so they can be re-added.
        $groups = $view_display->getThirdPartySettings('field_group');
        foreach (array_keys($groups) as $group_name) {
          $view_display->unsetThirdPartySetting('field_group', $group_name);
        }

        // Add groups to the view display.
        $this->componentWeight = 0;
        foreach ($settings['groups'] as $group_name => $group_settings) {
          // Check if the field should be added to the view display.
          if (!in_array($display_id, $group_settings['view_displays'] ?? [], TRUE)) {
            continue;
          }
          $this->addGroupToViewDisplay($view_display, $group_name, $settings);
        }

        // Fix the weights for all fields/subgroups of the groups. We do this
        // after adding all groups to the form display, because we need to know
        // all groups have been created.
        foreach ($settings['groups'] as $group_settings) {
          // Update the weight of the fields in the group.
          $weight = 0;
          foreach ($group_settings['fields'] ?? [] as $field_name) {
            // Check if we have a component, in that case we update the weight.
            $component = $view_display->getComponent($field_name);
            if ($component) {
              $component['weight'] = $weight++;
              $view_display->setComponent($field_name, $component);
              continue;
            }

            // If we don't have a component, we are probably working with a
            // group. In that case we update the weight of the group.
            $group = $view_display->getThirdPartySetting('field_group', $field_name);
            if ($group) {
              $group['weight'] = $weight++;
              $view_display->setThirdPartySetting('field_group', $field_name, $group);
            }
          }
        }
      }

      // Save the view display.
      $view_display->save();

      // Translate the view display.
      if ($this->moduleHandler->moduleExists('language')) {
        $this->translateViewDisplay($entity_type_id, $bundle_id, $display_id, $settings);
      }
    }

    // Delete the view displays that are not configured.
    $view_displays = $this->entityTypeManager->getStorage('entity_view_display')->loadByProperties([
      'targetEntityType' => $entity_type_id,
      'bundle' => $bundle_id,
    ]);
    foreach ($view_displays as $view_display) {
      if (!in_array($view_display->getMode(), $display_ids, TRUE)) {
        $view_display->delete();
      }
    }
  }

  /**
   * Adds a field to a view display.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param \Drupal\Core\Entity\Entity\EntityViewDisplay $view_display
   *   The view display.
   * @param string $field_name
   *   The field machine name.
   * @param array $settings
   *   The bundle settings.
   */
  public function addFieldToViewDisplay(string $entity_type_id, EntityViewDisplay $view_display, string $field_name, array $settings): void {
    $field_settings = $settings['fields'][$field_name];

    // Validate if the field has a valid formatter type.
    if (!isset($field_settings['formatter_type'])) {
      throw new \InvalidArgumentException(sprintf('The formatter type %s for field %s in entity type %s is not configured.', $field_settings['formatter_type'], $field_name, $entity_type_id));
    }

    // Get the component third party settings. We remove the settings for
    // disabled modules.
    if (isset($field_settings['field_third_party_settings'])) {
      foreach ($field_settings['field_third_party_settings'] as $module => $module_settings) {
        if (!$this->moduleHandler->moduleExists($module)) {
          unset($field_settings['field_third_party_settings'][$module]);
        }
      }
    }

    // Add the field to the form display.
    $view_display->setComponent($field_name, [
      'type' => $field_settings['formatter_type'],
      'label' => $field_settings['formatter_label'] ?? 'hidden',
      'settings' => $field_settings['formatter_settings'] ?? [],
      'third_party_settings' => $field_settings['formatter_third_party_settings'] ?? [],
      'weight' => $this->componentWeight++,
    ]);
  }

  /**
   * Adds a group to a view display.
   *
   * @param \Drupal\Core\Entity\Entity\EntityViewDisplay $view_display
   *   The view display.
   * @param string $group_name
   *   The group machine name.
   * @param array $settings
   *   The bundle settings.
   */
  public function addGroupToViewDisplay(EntityViewDisplay $view_display, string $group_name, array $settings): void {
    $group_settings = $settings['groups'][$group_name];

    $existing_group = $view_display->getThirdPartySetting('field_group', $group_name);
    $view_display->setThirdPartySetting('field_group', $group_name, [
      'label' => $group_settings['label'] ?? '',
      'parent_name' => $group_settings['parent'] ?? '',
      'region' => $group_settings['region'] ?? 'content',
      'weight' => $group_settings['weight'] ?? $existing_group['weight'] ?? $this->componentWeight++,
      'format_type' => $group_settings['group_type'],
      'format_settings' => $group_settings['settings'] ?? [],
      'children' => $group_settings['fields'] ?? [],
    ]);
  }

  /**
   * Translates the view display for the bundle.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $bundle_id
   *   The bundle ID.
   * @param string $display_id
   *   The form display ID.
   * @param array $settings
   *   The bundle settings.
   */
  protected function translateViewDisplay(string $entity_type_id, string $bundle_id, string $display_id, array $settings): void {
    /** @var \Drupal\language\ConfigurableLanguageManagerInterface $this->languageManager */
    $field_definitions = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle_id);
    foreach ($this->languageManager->getLanguages() as $language) {
      // Skip the default language.
      if ($language->getId() === $this->languageManager->getDefaultLanguage()->getId()) {
        continue;
      }

      // Translate the field settings in the view display.
      $config_translation = $this->languageManager->getLanguageConfigOverride($language->getId(), implode('.', [
        'core',
        'entity_view_display',
        $entity_type_id,
        $bundle_id,
        $display_id,
      ]));

      $translated_field_settings = [];
      foreach ($settings['fields'] as $field_name => $field_settings) {
        // Check if the field should be added to the view display.
        if (!in_array($display_id, $field_settings['view_displays'] ?? [], TRUE) || !$field_definitions[$field_name]->isDisplayConfigurable('view')) {
          continue;
        }

        // Check if the field is translated.
        if (!isset($settings['translations'][$language->getId()]['fields'][$field_name])) {
          continue;
        }

        $language_settings = $settings['translations'][$language->getId()]['fields'][$field_name];

        // Check if the field has translated formatter settings.
        if (isset($language_settings['formatter_settings'])) {
          $translated_field_settings[$field_name]['settings'] = $language_settings['formatter_settings'];
        }

        // Get the formatter third party settings. We remove the settings for
        // disabled modules.
        if (isset($language_settings['formatter_third_party_settings'])) {
          foreach ($language_settings['formatter_third_party_settings'] as $module => $module_settings) {
            if (!$this->moduleHandler->moduleExists($module)) {
              unset($language_settings['formatter_third_party_settings'][$module]);
            }
          }
        }

        // Check if the field has translated formatter third party settings.
        if (isset($language_settings['formatter_third_party_settings'])) {
          $translated_field_settings[$field_name]['third_party_settings'] = $language_settings['formatter_third_party_settings'];
        }
      }

      // Set the translated field settings.
      if (!empty($translated_field_settings)) {
        $config_translation->set('content', $translated_field_settings);
      }
      else {
        $config_translation->clear('content');
      }

      $translated_group_settings = [];
      foreach ($settings['groups'] as $group_name => $group_settings) {
        // Check if the group should be added to the view display.
        if (!in_array($display_id, $group_settings['view_displays'] ?? [], TRUE)) {
          continue;
        }

        // Check if the group is translated.
        if (!isset($settings['translations'][$language->getId()]['groups'][$group_name])) {
          continue;
        }

        $language_settings = $settings['translations'][$language->getId()]['groups'][$group_name];

        $translated_group_settings[$group_name] = array_filter([
          'label' => $language_settings['label'] ?? '',
          'format_settings' => array_filter($language_settings['settings'] ?? []),
        ]);
      }

      // Set the translated group settings.
      if (!empty($translated_group_settings)) {
        $config_translation->set('third_party_settings', [
          'field_group' => $translated_group_settings,
        ]);
      }
      else {
        $config_translation->clear('third_party_settings');
      }

      // Save the config translation when not empty.
      if ($config_translation->get()) {
        $config_translation->save();
      }
      else {
        $config_translation->delete();
      }
    }
  }

}
