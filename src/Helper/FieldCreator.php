<?php

namespace Drupal\yaml_bundles\Helper;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\Entity\BaseFieldOverride;
use Drupal\Core\Field\Plugin\Field\FieldType\StringItemBase;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\language\ConfigurableLanguageManagerInterface;
use Drupal\search_api\Entity\Index;
use Drupal\text\Plugin\Field\FieldType\TextItemBase;

/**
 * Helper service to create fields for entity bundles.
 */
class FieldCreator {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * The UUID service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected UuidInterface $uuidService;

  /**
   * Constructs a FieldCreator instance.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid_service
   *   The UUID service.
   */
  public function __construct(ModuleHandlerInterface $module_handler, LanguageManagerInterface $language_manager, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, UuidInterface $uuid_service) {
    $this->moduleHandler = $module_handler;
    $this->languageManager = $language_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->uuidService = $uuid_service;
  }

  /**
   * Creates a custom field for a bundle.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $bundle_id
   *   The bundle ID.
   * @param string $field_name
   *   The field name.
   * @param array $settings
   *   The bundle settings.
   */
  public function createField(string $entity_type_id, string $bundle_id, string $field_name, array $settings): void {
    $field_settings = $settings['fields'][$field_name];

    // Check if the field is a base field, we only need to create the field
    // storage and config for custom fields.
    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $base_fields */
    $base_fields = $this->entityFieldManager->getBaseFieldDefinitions($entity_type_id);
    if (!isset($base_fields[$field_name])) {

      // Create the field storage if it does not exist.
      $field_storage = FieldStorageConfig::loadByName($entity_type_id, $field_name);
      if (!$field_storage instanceof FieldStorageConfig) {
        /** @var \Drupal\field\Entity\FieldStorageConfig $field_storage */
        $field_storage = FieldStorageConfig::create([
          'uuid' => $this->uuidService->generate(),
          'entity_type' => $entity_type_id,
          'field_name' => $field_name,
          'type' => $field_settings['field_type'],
        ]);
      }

      // Update the field storage settings.
      $field_storage->set('langcode', $settings['langcode'] ?? $this->languageManager->getDefaultLanguage()->getId());
      $field_storage->setCardinality($field_settings['cardinality'] ?? 1);
      if (isset($field_settings['storage_settings'])) {
        $field_storage->setSettings($field_settings['storage_settings']);
      }
      // Remove settings if none were provided
      else {
        $field_storage->setSettings([]);
      }

      // Update the field storage third party settings. Check if a module is
      // enabled before the settings are applied.
      if (isset($field_settings['storage_third_party_settings'])) {
        foreach ($field_settings['storage_third_party_settings'] as $module => $module_settings) {
          if ($this->moduleHandler->moduleExists($module)) {
            foreach ($module_settings as $name => $value) {
              $field_storage->setThirdPartySetting($module, $name, $value);
            }
          }
        }
      }
      // Remove any third party settings not provided by our settings.
      $modules = $field_storage->getThirdPartyProviders();
      foreach ($modules as $module) {
        $settings = $field_storage->getThirdPartySettings($module);
        foreach (array_keys($settings) as $name) {
          if (!isset($field_settings['storage_third_party_settings'][$module][$name])) {
            $field_storage->unsetThirdPartySetting($module, $name);
          }
        }
      }

      // Update the field storage.
      $field_storage->save();

      // Create the field config if it does not exist.
      $field_config = FieldConfig::loadByName($entity_type_id, $bundle_id, $field_name);
      if (!$field_config instanceof FieldConfig) {
        /** @var \Drupal\field\Entity\FieldConfig $field_config */
        $field_config = FieldConfig::create([
          'uuid' => $this->uuidService->generate(),
          'entity_type' => $entity_type_id,
          'field_name' => $field_name,
          'bundle' => $bundle_id,
          'field_type' => $field_settings['field_type'],
        ]);
      }

      // Update the field config settings.
      $field_config->set('langcode', $settings['langcode'] ?? $this->languageManager->getDefaultLanguage()->getId());
      $field_config->setLabel($field_settings['label']);
      $field_config->setDescription($field_settings['description'] ?? '');
      $field_config->setRequired($field_settings['required'] ?? FALSE);
      $field_config->setTranslatable($field_settings['translatable'] ?? TRUE);
      $field_config->setDefaultValue($field_settings['field_default_value'] ?? NULL);
      if (isset($field_settings['field_settings'])) {
        $field_config->setSettings($field_settings['field_settings']);
      }
      // Remove settings if none were provided
      else {
        $field_config->setSettings([]);
      }

      // Update the field config third party settings. Check if a module is
      // enabled before the settings are applied.
      if (isset($field_settings['field_third_party_settings'])) {
        foreach ($field_settings['field_third_party_settings'] as $module => $module_settings) {
          if ($this->moduleHandler->moduleExists($module)) {
            foreach ($module_settings as $name => $value) {
              $field_config->setThirdPartySetting($module, $name, $value);
            }
          }
        }
      }
      // Remove any third party settings not provided by our settings.
      $modules = $field_config->getThirdPartyProviders();
      foreach ($modules as $module) {
        $settings = $field_config->getThirdPartySettings($module);
        foreach (array_keys($settings) as $name) {
          if (!isset($field_settings['field_third_party_settings'][$module][$name])) {
            $field_config->unsetThirdPartySetting($module, $name);
          }
        }
      }

      // Save the field config.
      $field_config->save();

      // Translate the field label and description.
      $this->translateFieldConfig($entity_type_id, $bundle_id, $field_name, $settings);

    }
    else {
      // Create a base field override if it does not exist.
      $base_field_override = BaseFieldOverride::loadByName($entity_type_id, $bundle_id, $field_name);
      if (!$base_field_override instanceof BaseFieldOverride && (isset($field_settings['label']) || isset($field_settings['description']))) {
        $base_field_override = BaseFieldOverride::createFromBaseFieldDefinition($base_fields[$field_name], $bundle_id);
        if (isset($field_settings['label'])) {
          $base_field_override->set('label', $field_settings['label']);
        }
        if (isset($field_settings['description'])) {
          $base_field_override->set('description', $field_settings['description'] ?? '');
        }
        $base_field_override->save();
      }

      // Translate the base field label.
      $this->translateBaseField($entity_type_id, $bundle_id, $field_name, $settings);
    }

    // Add the field to search API if configured and the search API module is
    // enabled.
    if (!empty($settings['search_indexes']) && $this->moduleHandler->moduleExists('search_api')) {
      $this->addFieldToSearchApi($entity_type_id, $bundle_id, $field_name, $settings);
    }
  }

  /**
   * Translates the field configuration.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $bundle_id
   *   The bundle ID.
   * @param string $field_name
   *   The field name.
   * @param array $settings
   *   The bundle settings.
   */
  protected function translateFieldConfig(string $entity_type_id, string $bundle_id, string $field_name, array $settings): void {
    if (!$this->languageManager instanceof ConfigurableLanguageManagerInterface) {
      return;
    }

    // Translate the field storage.
    foreach ($this->languageManager->getLanguages() as $language) {
      // Skip the default language.
      if ($language->getId() === $this->languageManager->getDefaultLanguage()->getId()) {
        continue;
      }

      // Check if the field is translated.
      if (!isset($settings['translations'][$language->getId()]['fields'][$field_name])) {
        continue;
      }

      $language_settings = $settings['translations'][$language->getId()]['fields'][$field_name];

      // Translate the field storage settings.
      $config_translation = $this->languageManager->getLanguageConfigOverride($language->getId(), implode('.', [
        'field',
        'storage',
        $entity_type_id,
        $field_name,
      ]));

      $language_settings['storage_settings'] = array_filter($language_settings['storage_settings'] ?? []);

      // Check if the field storage settings are translated.
      if (empty($language_settings['storage_settings'])) {
        $config_translation->clear('settings');
      }
      else {
        $config_translation->set('settings', $language_settings['storage_settings']);
      }

      // Save the config translation when not empty.
      if ($config_translation->get()) {
        $config_translation->save();
      }
      else {
        $config_translation->delete();
      }
    }

    // Translate the field config.
    foreach ($this->languageManager->getLanguages() as $language) {
      // Skip the default language.
      if ($language->getId() === $this->languageManager->getDefaultLanguage()->getId()) {
        continue;
      }

      // Check if the translation at least contains a label.
      if (!isset($settings['translations'][$language->getId()]['fields'][$field_name]['label'])) {
        continue;
      }

      $language_settings = $settings['translations'][$language->getId()]['fields'][$field_name];

      // Translate the field config label and description.
      $config_translation = $this->languageManager->getLanguageConfigOverride($language->getId(), implode('.', [
        'field',
        'field',
        $entity_type_id,
        $bundle_id,
        $field_name,
      ]));

      $config_translation->set('label', $language_settings['label']);

      $description = $language_settings['description'] ?? NULL;
      if ($description) {
        $config_translation->set('description', $description);
      }
      else {
        $config_translation->clear('description');
      }

      // Save the config translation when not empty.
      if ($config_translation->get()) {
        $config_translation->save();
      }
      else {
        $config_translation->delete();
      }
    }
  }

  /**
   * Translates the base field configuration.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $bundle_id
   *   The bundle ID.
   * @param string $field_name
   *   The field name.
   * @param array $settings
   *   The bundle settings.
   */
  protected function translateBaseField(string $entity_type_id, string $bundle_id, string $field_name, array $settings): void {
    if (!$this->languageManager instanceof ConfigurableLanguageManagerInterface) {
      return;
    }

    foreach ($this->languageManager->getLanguages() as $language) {
      // Skip the default language.
      if ($language->getId() === $this->languageManager->getDefaultLanguage()->getId()) {
        continue;
      }

      // Check if the translation at least contains a label.
      if (!isset($settings['translations'][$language->getId()]['fields'][$field_name]['label'])) {
        continue;
      }

      $language_settings = $settings['translations'][$language->getId()]['fields'][$field_name];

      // Translate the base field label.
      $config_translation = $this->languageManager->getLanguageConfigOverride($language->getId(), implode('.', [
        'core',
        'base_field_override',
        $entity_type_id,
        $bundle_id,
        $field_name,
      ]));
      $config_translation->set('label', $language_settings['label']);
      $config_translation->set('description', $language_settings['description'] ?? '');
      $config_translation->save();
    }
  }

  /**
   * Adds a field to search API.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $bundle_id
   *   The bundle ID.
   * @param string $field_name
   *   The field name.
   * @param array $settings
   *   The bundle settings.
   */
  protected function addFieldToSearchApi(string $entity_type_id, string $bundle_id, string $field_name, array $settings): void {
    $field_definition = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle_id)[$field_name];
    $field_settings = $settings['fields'][$field_name];

    $allow_search = $field_settings['search'] ?? FALSE;
    if (!$allow_search) {
      return;
    }

    /** @var \Drupal\search_api\Utility\FieldsHelperInterface $fields_helper */
    /** @var \Drupal\search_api\Utility\DataTypeHelper $data_type_helper */
    // We can not use dependency injection here because the search API module
    // is optional. We check that before calling this method.
    // @phpstan-ignore-next-line
    $fields_helper = \Drupal::service('search_api.fields_helper');
    // We can not use dependency injection here because the search API module
    // is optional. We check that before calling this method.
    // @phpstan-ignore-next-line
    $data_type_helper = \Drupal::service('search_api.data_type_helper');

    foreach ($settings['search_indexes'] as $index_id) {
      $search_index = $this->entityTypeManager->getStorage('search_api_index')->load($index_id);

      // Check if the index exists.
      if (!$search_index instanceof Index) {
        continue;
      }

      // Fetch the field data type from the main property definition.
      $main_property = $field_definition->getFieldStorageDefinition()->getMainPropertyName();
      $original_data_type = $field_definition->getFieldStorageDefinition()->getPropertyDefinitions()[$main_property]->getDataType();
      $data_type = $data_type_helper->getFieldTypeMapping()[$original_data_type] ?? NULL;

      // Check if the data type is valid, if not save the index with the field
      // removed and continue.
      if (!$data_type) {
        $search_index->save();
        continue;
      }

      // Try to fetch the existing field. Remove any existing field to let it be
      // recreated.
      $search_index->removeField($field_name);
      $field = $fields_helper->createFieldFromProperty($search_index, $field_definition, 'entity:' . $entity_type_id, $field_name, $field_name, $data_type);
      $field->setLabel($field_definition->getLabel());
      $search_index->addField($field);

      // For text fields, we also a fulltext and ngram versions to allow
      // searching by (partial) keyword.
      $field_class = $field_definition->getItemDefinition()->getClass();
      $is_text_field = is_subclass_of($field_class, StringItemBase::class) || is_subclass_of($field_class, TextItemBase::class);
      if ($is_text_field) {
        $fulltext_field_name = $field_name . '_fulltext';
        $search_index->removeField($fulltext_field_name);
        $field = $fields_helper->createFieldFromProperty($search_index, $field_definition, 'entity:' . $entity_type_id, $field_name, $fulltext_field_name, 'solr_text_omit_norms');
        $field->setLabel($field_definition->getLabel() . ' (fulltext)');
        $field->setBoost($field_settings['search_boost'] ?? 1);
        $search_index->addField($field);

        // Add an ngram field if enabled, defaults to TRUE.
        $allow_ngram = $field_settings['search_partial'] ?? TRUE;
        if ($allow_ngram) {
          $ngram_field_name = $field_name . '_ngram';
          $search_index->removeField($ngram_field_name);
          $field = $fields_helper->createFieldFromProperty($search_index, $field_definition, 'entity:' . $entity_type_id, $field_name, $ngram_field_name, 'solr_text_custom_omit_norms:edge');
          $field->setLabel($field_definition->getLabel() . ' (ngram)');
          $boost = ($field_settings['search_boost'] ?? 1) / 2;
          $field->setBoost(($boost > 1) ? $boost : 1);
          $search_index->addField($field);
        }
      }

      // Allow boosting for date fields.
      if ('datetime' === $field_definition->getFieldStorageDefinition()->getType()) {
        if (isset($field_settings['search_boost'], $search_index->getProcessors()['solr_boost_more_recent'])) {
          $configuration = $search_index->getProcessor('solr_boost_more_recent')->getConfiguration();
          $configuration['boosts'][$field_name] = [
            'boost' => 0.5,
            'resolution' => 'NOW',
            'm' => '3.16e-11',
            'a' => 0.1,
            'b' => 0.05,
          ];
          $search_index->getProcessor('solr_boost_more_recent')->setConfiguration($configuration);
        }
        elseif (isset($search_index->getProcessors()['solr_boost_more_recent'])) {
          $configuration = $search_index->getProcessor('solr_boost_more_recent')->getConfiguration();
          unset($configuration['boosts'][$field_name]);
          $search_index->getProcessor('solr_boost_more_recent')->setConfiguration($configuration);
        }
      }

      $search_index->save();
    }
  }

}
