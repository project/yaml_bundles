<?php

namespace Drupal\yaml_bundles;

use Drupal\content_moderation\Plugin\WorkflowType\ContentModerationInterface;
use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\language\ConfigurableLanguageManagerInterface;
use Drupal\pathauto\PathautoPatternInterface;
use Drupal\search_api\Entity\Index;
use Drupal\workflows\Entity\Workflow;
use Drupal\yaml_bundles\Helper\DisplayCreator;
use Drupal\yaml_bundles\Helper\FieldCreator;
use Drupal\yaml_bundles\Helper\SettingsCreator;
use Drupal\yaml_bundles\Plugin\BundlePluginManager;

/**
 * Service to create bundles for entity types.
 */
class BundleCreator {

  /**
   * The bundle plugin manager.
   *
   * @var \Drupal\yaml_bundles\Plugin\BundlePluginManager
   */
  protected BundlePluginManager $bundlePluginManager;

  /**
   * The bundle settings creator.
   *
   * @var \Drupal\yaml_bundles\Helper\SettingsCreator
   */
  protected SettingsCreator $settingsCreator;

  /**
   * The bundle field creator.
   *
   * @var \Drupal\yaml_bundles\Helper\FieldCreator
   */
  protected FieldCreator $fieldCreator;

  /**
   * The bundle display creator.
   *
   * @var \Drupal\yaml_bundles\Helper\DisplayCreator
   */
  protected DisplayCreator $displayCreator;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * Constructs a BundleCreator instance.
   *
   * @param \Drupal\yaml_bundles\Plugin\BundlePluginManager $bundle_plugin_manager
   *   The bundle plugin manager.
   * @param \Drupal\yaml_bundles\Helper\SettingsCreator $settings_creator
   *   The bundle settings creator.
   * @param \Drupal\yaml_bundles\Helper\FieldCreator $field_creator
   *   The bundle field creator.
   * @param \Drupal\yaml_bundles\Helper\DisplayCreator $display_creator
   *   The bundle display creator.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(BundlePluginManager $bundle_plugin_manager, SettingsCreator $settings_creator, FieldCreator $field_creator, DisplayCreator $display_creator, ModuleHandlerInterface $module_handler, LanguageManagerInterface $language_manager, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager) {
    $this->bundlePluginManager = $bundle_plugin_manager;
    $this->settingsCreator = $settings_creator;
    $this->fieldCreator = $field_creator;
    $this->displayCreator = $display_creator;
    $this->moduleHandler = $module_handler;
    $this->languageManager = $language_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * Gets the custom YAML bundles per entity type.
   *
   * @param bool $merge_settings
   *   Whether to merge the default settings with the custom settings.
   *
   * @return array
   *   The custom YAML bundles per entity type.
   */
  public function getBundleDefinitions(bool $merge_settings = FALSE): array {
    $definitions = [];
    $bundle_definitions = $this->bundlePluginManager->getDefinitions();
    foreach ($bundle_definitions as $plugin_id => $settings) {
      // When the plugin is created by a deriver, the plugin ID is prefixed with
      // the deriver ID. We need to remove the deriver ID to get the entity type
      // and bundle IDs.
      if (isset($settings['deriver'])) {
        [, $plugin_id] = explode(':', $plugin_id);
      }
      [$entity_type_id, $bundle_id] = explode('.', $plugin_id);

      // Check if the entity type exists.
      if (!$this->entityTypeManager->hasDefinition($entity_type_id)) {
        continue;
      }

      // Merge the available default settings from the entity type, bundle and
      // field plugins.
      if ($merge_settings) {
        $settings = $this->settingsCreator->finalizeSettings($entity_type_id, $settings);
      }

      $definitions[$entity_type_id][$bundle_id] = $settings;
    }
    return $definitions;
  }

  /**
   * Creates the bundles for the yaml_bundles.bundle plugins.
   */
  public function createBundles(): void {
    foreach ($this->getBundleDefinitions(TRUE) as $entity_type_id => $bundles) {
      foreach ($bundles as $bundle_id => $settings) {
        $this->createBundle($entity_type_id, $bundle_id, $settings);
      }
    }
  }

  /**
   * Creates an entity bundle.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $bundle_id
   *   The bundle ID.
   * @param array $settings
   *   The bundle settings.
   *
   * @throws \InvalidArgumentException
   */
  public function createBundle(string $entity_type_id, string $bundle_id, array $settings): void {
    // Check if the entity type exists.
    if (!$this->entityTypeManager->hasDefinition($entity_type_id)) {
      throw new \InvalidArgumentException(sprintf('The entity type %s does not exist.', $entity_type_id));
    }

    // Get the entity type and bundle entity type.
    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
    $bundle_entity_type_id = $entity_type->getBundleEntityType();

    // Check if the entity type allows bundles.
    if (!$bundle_entity_type_id) {
      throw new \InvalidArgumentException(sprintf('The entity type %s does not support bundles.', $entity_type_id));
    }

    // Fetch the bundle entity type.
    $bundle_entity_type = $this->entityTypeManager->getDefinition($bundle_entity_type_id);

    // Create the bundle is it doesn't exist yet.
    $bundle = $this->entityTypeManager->getStorage($bundle_entity_type_id)->load($bundle_id);
    if (!$bundle instanceof ConfigEntityBundleBase) {
      /** @var \Drupal\Core\Config\Entity\ConfigEntityBundleBase $bundle */
      $bundle = $this->entityTypeManager->getStorage($bundle_entity_type_id)->create([
        $bundle_entity_type->getKey('id') => $bundle_id,
      ]);
    }

    // Fetch the allowed configuration keys for the bundle.
    $allowed_keys = $bundle_entity_type->get('config_export');

    // Don't allow to change the bundle ID.
    $allowed_keys = array_diff($allowed_keys, [$bundle_entity_type->getKey('id')]);

    // Update the minimal bundle configuration.
    $bundle->set($bundle_entity_type->getKey('label'), $settings['label'] ?? '');
    if (in_array('description', $allowed_keys, TRUE)) {
      $bundle->set('description', $settings['description'] ?? '');
    }
    $bundle->set('langcode', $settings['langcode'] ?? $this->languageManager->getDefaultLanguage()->getId());

    // Set the custom bundle settings.
    foreach ($settings as $key => $value) {
      if (in_array($key, $allowed_keys, TRUE)) {
        $bundle->set($key, $value);
      }
    }

    // Save the updated bundle.
    $bundle->save();

    // Translate the bundle configuration.
    $this->translateBundleConfig($entity_type_id, $bundle_id, $settings);

    // Add content translation settings for the bundle when the
    // content_translation module is enabled.
    if ($this->moduleHandler->moduleExists('content_translation')) {
      $this->enableContentTranslation($entity_type_id, $bundle_id, $settings);
    }

    // Enable a workflow if configured and the content_moderation and workflows
    // modules are enabled.
    if (!empty($settings['workflow']) && $this->moduleHandler->moduleExists('content_moderation') && $this->moduleHandler->moduleExists('workflows')) {
      $this->enableWorkflow($entity_type_id, $bundle_id, $settings);
    }

    // Create a path alias if we have a custom path and the pathauto module is
    // enabled.
    if (!empty($settings['path']) && $this->moduleHandler->moduleExists('pathauto')) {
      $this->createPathAlias($entity_type_id, $bundle_id, $settings);
    }

    // Add simple sitemap settings if configured and the simple_sitemap module
    // is enabled.
    if (isset($settings['sitemap']) && $this->moduleHandler->moduleExists('simple_sitemap')) {
      // @phpstan-ignore-next-line
      \Drupal::service('simple_sitemap.entity_manager')->setBundleSettings($entity_type_id, $bundle_id, $settings['sitemap']);
    }

    // Boosts a bundle in search API if configured and the search_api module is
    // enabled.
    if (!empty($settings['search_indexes']) && $this->moduleHandler->moduleExists('search_api')) {
      $this->configureSearchApi($entity_type_id, $bundle_id, $settings);
    }

    // Create the fields for the bundle.
    foreach (array_keys($settings['fields']) as $field_name) {
      $this->fieldCreator->createField($entity_type_id, $bundle_id, $field_name, $settings);
    }

    // Delete the fields that are not configured.
    foreach ($this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle_id) as $field_definition) {
      // Only delete custom field configurations.
      if ($field_definition instanceof FieldConfig && !isset($settings['fields'][$field_definition->getName()])) {
        $field_definition->delete();
      }
    }

    // Create the form and view displays.
    $this->displayCreator->createFormDisplays($entity_type_id, $bundle_id, $settings);
    $this->displayCreator->createViewDisplays($entity_type_id, $bundle_id, $settings);
  }

  /**
   * Translates the bundle configuration.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $bundle_id
   *   The bundle ID.
   * @param array $settings
   *   The bundle settings.
   */
  protected function translateBundleConfig(string $entity_type_id, string $bundle_id, array $settings): void {
    if (!$this->languageManager instanceof ConfigurableLanguageManagerInterface) {
      return;
    }

    // Get the entity type and bundle entity type.
    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
    /** @var \Drupal\Core\Config\Entity\ConfigEntityTypeInterface $bundle_entity_type */
    $bundle_entity_type = $this->entityTypeManager->getDefinition($entity_type->getBundleEntityType());

    // Fetch the allowed configuration keys for the bundle.
    $allowed_keys = $bundle_entity_type->get('config_export');

    foreach ($this->languageManager->getLanguages() as $language) {
      // Skip the default language.
      if ($language->getId() === $this->languageManager->getDefaultLanguage()->getId()) {
        continue;
      }

      // Check if the translation at least contains a label.
      if (!isset($settings['translations'][$language->getId()]['label'])) {
        continue;
      }

      $language_settings = $settings['translations'][$language->getId()];
      $config_translation = $this->languageManager->getLanguageConfigOverride($language->getId(), implode('.', [
        $bundle_entity_type->getConfigPrefix(),
        $bundle_id,
      ]));

      $config_translation->set($bundle_entity_type->getKey('label'), $language_settings['label']);

      $description = $language_settings['description'] ?? NULL;
      if (in_array('description', $allowed_keys, TRUE) && $description) {
        $config_translation->set('description', $description);
      }
      else {
        $config_translation->clear('description');
      }

      // Save the config translation when not empty.
      if ($config_translation->get()) {
        $config_translation->save();
      }
      else {
        $config_translation->delete();
      }
    }
  }

  /**
   * Enables content translation for the bundle.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $bundle_id
   *   The bundle ID.
   * @param array $settings
   *   The bundle settings.
   */
  protected function enableContentTranslation(string $entity_type_id, string $bundle_id, array $settings): void {
    // Load the existing config entity or create a new one.
    $config = $this->entityTypeManager->getStorage('language_content_settings')->load(implode('.', [
      $entity_type_id,
      $bundle_id,
    ]));
    if (!$config) {
      $config = $this->entityTypeManager->getStorage('language_content_settings')->create([
        'target_entity_type_id' => $entity_type_id,
        'target_bundle' => $bundle_id,
      ]);
    }

    // Update the content translation settings.
    /** @var \Drupal\language\Entity\ContentLanguageSettings $config */
    $config->set('langcode', $settings['langcode'] ?? $this->languageManager->getDefaultLanguage()->getId());
    $config->setDefaultLangcode($settings['content_translation']['default_langcode'] ?? LanguageInterface::LANGCODE_SITE_DEFAULT);
    $config->setThirdPartySetting('content_translation', 'enabled', (bool) ($settings['content_translation']['enabled'] ?? TRUE));
    $config->setThirdPartySetting('content_translation', 'bundle_settings', [
      'untranslatable_fields_hide' => (string) ($settings['content_translation']['bundle_settings']['untranslatable_fields_hide'] ?? '1'),
    ]);
    $config->setLanguageAlterable((bool) ($settings['content_translation']['language_alterable'] ?? TRUE));
    $config->save();
  }

  /**
   * Enables a workflow for the bundle.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $bundle_id
   *   The bundle ID.
   * @param array $settings
   *   The bundle settings.
   */
  protected function enableWorkflow(string $entity_type_id, string $bundle_id, array $settings): void {
    // Check if the workflow exists.
    $workflow = $this->entityTypeManager->getStorage('workflow')->load($settings['workflow']);
    if (!$workflow instanceof Workflow || !$workflow->getTypePlugin() instanceof ContentModerationInterface) {
      return;
    }

    // Add the bundle to the workflow if it is not already added.
    $workflow->getTypePlugin()->addEntityTypeAndBundle($entity_type_id, $bundle_id);
    $workflow->save();
  }

  /**
   * Creates a custom path alias for the bundle.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $bundle_id
   *   The bundle ID.
   * @param array $settings
   *   The bundle settings.
   */
  protected function createPathAlias(string $entity_type_id, string $bundle_id, array $settings): void {
    // Load the existing pattern entity or create a new one.
    $pattern_id = $entity_type_id . '_' . $bundle_id;
    $pattern = $this->entityTypeManager->getStorage('pathauto_pattern')->load($pattern_id);
    if (!$pattern instanceof PathautoPatternInterface) {
      /** @var \Drupal\pathauto\PathautoPatternInterface $pattern */
      $pattern = $this->entityTypeManager->getStorage('pathauto_pattern')->create([
        'id' => $pattern_id,
      ]);
      $pattern->set('type', 'canonical_entities:' . $entity_type_id);
      $pattern->addSelectionCondition([
        'id' => 'entity_bundle:' . $entity_type_id,
        'bundles' => [$bundle_id => $bundle_id],
        'negate' => FALSE,
        'context_mapping' => [$entity_type_id => $entity_type_id],
      ]);
    }

    // Update the pattern settings.
    $pattern->set('langcode', $settings['langcode'] ?? $this->languageManager->getDefaultLanguage()->getId());
    $pattern->set('label', ucfirst($entity_type_id) . ': ' . ucfirst($bundle_id));
    $pattern->setPattern($settings['path']);
    $pattern->save();
  }

  /**
   * Configure the search API settings.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string $bundle_id
   *   The bundle ID.
   * @param array $settings
   *   The bundle settings.
   */
  protected function configureSearchApi(string $entity_type_id, string $bundle_id, array $settings): void {
    foreach ($settings['search_indexes'] as $index_id) {
      $search_index = $this->entityTypeManager->getStorage('search_api_index')->load($index_id);

      // Check if the index exists.
      if (!$search_index instanceof Index || !isset($search_index->getProcessors()['type_boost'])) {
        continue;
      }

      // Add a boost for the content type.
      $boost = number_format((float) ($settings['boost'] ?? 1), 1, '.', '');
      $configuration = $search_index->getProcessor('type_boost')->getConfiguration();
      $configuration['boosts']['entity:' . $entity_type_id]['bundle_boosts'][$bundle_id] = (float) $boost;
      $search_index->getProcessor('type_boost')->setConfiguration($configuration);

      // Set the "full" view mode for rendered content in search API when
      // configured.
      $search_api_fields = $search_index->getFields();
      if (isset($search_api_fields['rendered_item'])) {
        $rendered_item_field = $search_api_fields['rendered_item'];
        $field_configuration = $rendered_item_field->getConfiguration();
        $field_configuration['view_mode']['entity:' . $entity_type_id][$bundle_id] = 'full';
        $rendered_item_field->setConfiguration($field_configuration);
        $search_index->setFields($search_api_fields);
      }

      $search_index->save();
    }
  }

}
