<?php

namespace Drupal\yaml_bundles\Plugin\Discovery;

use Drupal\Core\Plugin\Discovery\YamlDiscovery;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Prioritize weights while using YAML files to define plugin definitions.
 */
class WeightYamlDiscovery extends YamlDiscovery {

  /**
   * {@inheritdoc}
   */
  public function getDefinitions() {
    $plugins = $this->discovery->findAll();

    // Flatten definitions into what's expected from plugins.
    $definitions = [];
    foreach ($plugins as $provider => $list) {
      foreach ($list as $id => $definition) {
        // Check if the plugin already exists with a higher weight.
        if (isset($definitions[$id]) && ($definitions[$id]['weight'] ?? 0) > ($definition['weight'] ?? 0)) {
          continue;
        }

        // Add TranslatableMarkup.
        foreach ($this->translatableProperties as $property => $context_key) {
          if (isset($definition[$property])) {
            $options = [];
            // Move the t() context from the definition to the translation
            // wrapper.
            if ($context_key && isset($definition[$context_key])) {
              $options['context'] = $definition[$context_key];
              unset($definition[$context_key]);
            }
            // phpcs:ignore
            $definition[$property] = new TranslatableMarkup($definition[$property], [], $options);
          }
        }
        // Add ID and provider.
        $definitions[$id] = $definition + [
          'provider' => $provider,
          'id' => $id,
        ];
      }
    }

    return $definitions;
  }

}
