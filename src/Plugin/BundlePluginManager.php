<?php

namespace Drupal\yaml_bundles\Plugin;

use Drupal\Component\Plugin\Discovery\DiscoveryInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Plugin\Discovery\ContainerDerivativeDiscoveryDecorator;
use Drupal\Core\Plugin\Factory\ContainerFactory;
use Drupal\yaml_bundles\Plugin\Discovery\WeightYamlDiscovery;

/**
 * Defines a plugin manager for yaml_bundles.bundle plugins.
 *
 * Modules can define yaml_bundles.bundle plugins in a
 * MODULE_NAME.yaml_bundles.bundles.yml file contained in the module's base
 * directory. Each yaml_bundles.bundle has the following structure:
 *
 * @code
 *   ENTITY_TYPE_ID.BUNDLE_ID:
 *     label: [value]
 *     (optional) description: [value]
 *     (optional) langcode: [value] - Falls back to the default language.
 *     (optional) content_translation:
 *       enabled: [value]
 *       bundle_settings:
 *         untranslatable_fields_hide: [value]
 *       language_alterable: [value]
 *     (optional) form_displays:
 *       - [value]
 *     (optional) view_displays:
 *       - [value]
 *     (optional) fields:
 *       BASE_FIELD_NAME:
 *         (optional) form_displays:
 *           - [value]
 *         (optional) view_displays:
 *           - [value]
 *       field_FIELD_NAME:
 *         type: [value] - Must be a valid field type supported by plugin.manager.yaml_bundles.field_type.
 *         label: [value]
 *         (optional) description: [value]
 *         (optional) required: [value] - Defaults to false.
 *         (optional) translatable: [value] - Defaults to true.
 *         (optional) cardinality: [value] - Defaults to 1. Set to -1 for unlimited.
 *         (optional) storage_settings: [settings for the field storage]
 *         (optional) storage_third_party_settings: [third party settings for the field storage]
 *         (optional) field_default_value: [default value for the field]
 *         (optional) field_settings: [settings for the field config]
 *         (optional) field_third_party_settings: [third party settings for the field config]
 *         (optional) widget_type: [field widget type]
 *         (optional) widget_weight: [field widget weight] - Defaults to order of the fields.
 *         (optional) widget_settings: [settings for the field widget]
 *         (optional) widget_third_party_settings: [third party settings for the field widget]
 *         (optional) formatter_type: [field formatter type]
 *         (optional) formatter_label: [field formatter label display]
 *         (optional) formatter_weight: [field formatter weight] - Defaults to order of the fields.
 *         (optional) formatter_settings: [settings for the field formatter]
 *         (optional) formatter_third_party_settings: [third party settings for the field formatter]
 *         (optional) form_displays:
 *           - [value] - When adding a base field to a display, the field must be display configurable (a node title for example can not be added to a view display).
 *         (optional) view_displays:
 *           - [value] - When adding a base field to a display, the field must be display configurable (a node title for example can not be added to a view display).
 *       ...
 *       ...
 *     (optional) translations:
 *       LANGUAGE_CODE:
 *         label: [value]
 *         (optional) description: [value]
 *         (optional) fields:
 *           field_FIELD_NAME:
 *             label: [value]
 *             (optional) description: [value]
 *           ...
 *           ...
 * @endcode
 *
 * For example:
 *
 * @code
 *   node.page:
 *     label: Page
 *     description: A description for the page content type.
 *     langcode: en
 *     help: ''
 *     new_revision: true
 *     preview_mode: 0
 *     display_submitted: false
 *     content_translation:
 *       enabled: true
 *       bundle_settings:
 *         untranslatable_fields_hide: '1'
 *       language_alterable: true
 *     form_displays:
 *       - short
 *     view_displays:
 *       - full
 *       - teaser
 *     fields:
 *       title:
 *         form_displays:
 *           - default
 *           - short
 *         view_displays:
 *           - default
 *           - teaser
 *       field_summary:
 *         type: text_long
 *         label: Summary
 *         description: This is a description for the summary.
 *         required: false
 *         form_displays:
 *           - default
 *           - short
 *         view_displays:
 *           - default
 *           - teaser
 *       field_body:
 *         type: text_long
 *         label: Text
 *         required: true
 *         form_displays:
 *           - default
 *         view_displays:
 *           - default
 *           - full
 *       field_image:
 *         type: entity_reference
 *         label: Image
 *         required: true
 *         storage_settings:
 *           target_type: media
 *         field_settings:
 *           handler: default:media
 *           handler_settings:
 *             target_bundles:
 *               - image
 *         widget_type: media_library_widget
 *         widget_settings:
 *           media_types:
 *           - image
 *         form_displays:
 *           - default
 *           - short
 *         view_displays:
 *           - default
 *           - full
 *           - teaser
 *     translations:
 *       nl:
 *         label: Pagina
 *         description: Een beschrijving voor het pagina inhoudstype.
 *         fields:
 *           field_summary:
 *             label: Samenvatting
 *             description: Dit is een beschrijving voor de samenvatting.
 *           field_body:
 *             label: Tekst
 *           field_image:
 *             label: Afbeelding
 * @endcode
 *
 * @see plugin_api
 */
class BundlePluginManager extends DefaultPluginManager {

  /**
   * Constructs a BundlePluginManager instance.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   */
  public function __construct(ModuleHandlerInterface $module_handler, CacheBackendInterface $cache_backend) {
    $this->factory = new ContainerFactory($this);
    $this->moduleHandler = $module_handler;
    $this->alterInfo('yaml_bundles_bundle_info');
    $this->setCacheBackend($cache_backend, 'yaml_bundles_bundle_plugins');
  }

  /**
   * {@inheritdoc}
   */
  protected function getDiscovery(): DiscoveryInterface {
    // @phpstan-ignore-next-line
    if (!isset($this->discovery)) {
      $yaml_discovery = new WeightYamlDiscovery('yaml_bundles.bundle', $this->moduleHandler->getModuleDirectories());
      $this->discovery = new ContainerDerivativeDiscoveryDecorator($yaml_discovery);
    }
    return $this->discovery;
  }

  /**
   * Gets the plugin definitions for a specific entity type.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   *
   * @return array
   *   An array of plugin definitions for the entoty type.
   */
  public function getDefinitionsByEntityType(string $entity_type_id): array {
    $definitions = [];
    foreach ($this->getDefinitions() as $plugin_id => $definition) {
      // When the plugin is created by a deriver, the plugin ID is prefixed with
      // the deriver ID. We need to remove the deriver ID to get the entity type
      // and bundle IDs.
      if (isset($definition['deriver'])) {
        [, $plugin_id] = explode(':', $plugin_id);
      }
      [$plugin_entity_type_id] = explode('.', $plugin_id);

      // Only return the plugin definitions for the given entity type.
      if ($plugin_entity_type_id === $entity_type_id) {
        $definitions[$plugin_id] = $definition;
      }
    }
    return $definitions;
  }

}
