<?php

namespace Drupal\yaml_bundles\Plugin;

use Drupal\Component\Plugin\Discovery\DiscoveryInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Plugin\Discovery\ContainerDerivativeDiscoveryDecorator;
use Drupal\Core\Plugin\Factory\ContainerFactory;
use Drupal\yaml_bundles\Plugin\Discovery\WeightYamlDiscovery;

/**
 * Defines a plugin manager for yaml_bundles.entity_type plugins.
 *
 * Modules can define yaml_bundles.entity_type plugins in a
 * MODULE_NAME.yaml_bundles.entity_type.yml file contained in the module's base
 * directory. The entity type plugins provide default settings which can be
 * overridden by the settings in the yaml_bundles.bundle plugins. Each
 * yaml_bundles.entity_type has the following structure:
 *
 * @code
 *   ENTITY_TYPE_ID:
 *     [key]: [value]
 * @endcode
 *
 * For example:
 *
 * @code
 *   node:
 *     new_revision: true
 * @endcode
 *
 * @see plugin_api
 */
class EntityTypePluginManager extends DefaultPluginManager {

  /**
   * Define required keys for entity type bundle plugins.
   */
  const REQUIRED_KEYS = [
    'media' => [
      'source',
    ],
  ];

  /**
   * Constructs an EntityTypePluginManager instance.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   */
  public function __construct(ModuleHandlerInterface $module_handler, CacheBackendInterface $cache_backend) {
    $this->factory = new ContainerFactory($this);
    $this->moduleHandler = $module_handler;
    $this->alterInfo('yaml_bundles_entity_type_info');
    $this->setCacheBackend($cache_backend, 'yaml_bundles_entity_type_plugins');
  }

  /**
   * {@inheritdoc}
   */
  protected function getDiscovery(): DiscoveryInterface {
    // @phpstan-ignore-next-line
    if (!isset($this->discovery)) {
      $yaml_discovery = new WeightYamlDiscovery('yaml_bundles.entity_type', $this->moduleHandler->getModuleDirectories());
      $this->discovery = new ContainerDerivativeDiscoveryDecorator($yaml_discovery);
    }
    return $this->discovery;
  }

}
