<?php

namespace Drupal\yaml_bundles\Plugin;

use Drupal\Component\Plugin\Discovery\DiscoveryInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Plugin\Discovery\ContainerDerivativeDiscoveryDecorator;
use Drupal\Core\Plugin\Factory\ContainerFactory;
use Drupal\yaml_bundles\Plugin\Discovery\WeightYamlDiscovery;

/**
 * Defines a plugin manager for yaml_bundles.group_type plugins.
 *
 * Modules can define yaml_bundles.group_type plugins in a
 * MODULE_NAME.yaml_bundles.group_type.yml file contained in the module's base
 * directory. The group type plugins provide default settings which can be
 * overridden by the groups in the yaml_bundles.bundle plugins. Each
 * yaml_bundles.group_type has the following structure:
 *
 * @code
 *   GROUP_TYPE:
 *     (optional) settings: [settings for the group type]
 * @endcode
 *
 * For example:
 *
 * @code
 *   tab:
 *     settings:
 *      id: ''
 *      description: ''
 *      classes: ''
 *      show_empty_fields: false
 *      required_fields: true
 *      formatter: closed
 *      direction: horizontal
 *      width_breakpoint: 640
 * @endcode
 *
 * @see plugin_api
 */
class GroupTypePluginManager extends DefaultPluginManager {

  /**
   * Constructs a GroupTypePluginManager instance.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   */
  public function __construct(ModuleHandlerInterface $module_handler, CacheBackendInterface $cache_backend) {
    $this->factory = new ContainerFactory($this);
    $this->moduleHandler = $module_handler;
    $this->alterInfo('yaml_bundles_group_type_info');
    $this->setCacheBackend($cache_backend, 'yaml_bundles_group_type_plugins');
  }

  /**
   * {@inheritdoc}
   */
  protected function getDiscovery(): DiscoveryInterface {
    // @phpstan-ignore-next-line
    if (!isset($this->discovery)) {
      $yaml_discovery = new WeightYamlDiscovery('yaml_bundles.group_type', $this->moduleHandler->getModuleDirectories());
      $this->discovery = new ContainerDerivativeDiscoveryDecorator($yaml_discovery);
    }
    return $this->discovery;
  }

}
