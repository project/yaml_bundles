<?php

namespace Drupal\yaml_bundles\Plugin;

use Drupal\Component\Plugin\Discovery\DiscoveryInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Plugin\Discovery\ContainerDerivativeDiscoveryDecorator;
use Drupal\Core\Plugin\Factory\ContainerFactory;
use Drupal\yaml_bundles\Plugin\Discovery\WeightYamlDiscovery;

/**
 * Defines a plugin manager for yaml_bundles.field_type plugins.
 *
 * Modules can define yaml_bundles.field_type plugins in a
 * MODULE_NAME.yaml_bundles.field_type.yml file contained in the module's base
 * directory. The field type plugins provide default settings which can be
 * overridden by the fields in the yaml_bundles.bundle plugins. Each
 * yaml_bundles.field_type has the following structure:
 *
 * @code
 *   FIELD_TYPE:
 *     (optional) storage_settings: [settings for the field storage]
 *     (optional) storage_third_party_settings: [third party settings for the field storage]
 *     (optional) field_default_value: [default value for the field]
 *     (optional) field_settings: [settings for the field config]
 *     (optional) field_third_party_settings: [third party settings for the field config]
 *     (optional) widget_type: [field widget type]
 *     (optional) widget_weight: [field widget weight]
 *     (optional) widget_settings: [settings for the field widget]
 *     (optional) widget_third_party_settings: [third party settings for the field widget]
 *     (optional) formatter_type: [field formatter type]
 *     (optional) formatter_label: [field formatter label display]
 *     (optional) formatter_weight: [field formatter weight]
 *     (optional) formatter_settings: [settings for the field formatter]
 *     (optional) formatter_third_party_settings: [third party settings for the field formatter]
 * @endcode
 *
 * For example:
 *
 * @code
 *   text_long:
 *     field_settings:
 *       allowed_formats:
 *         - basic_html
 *     widget_type: text_textarea
 *     widget_settings:
 *       rows: 5
 *     widget_third_party_settings:
 *       allowed_formats:
 *         hide_help: '1'
 *         hide_guidelines: '1'
 *       maxlength:
 *         maxlength_js: null
 *         maxlength_js_label: 'Content limited to @limit characters, remaining: <strong>@remaining</strong>'
 *         maxlength_js_enforce: false
 * @endcode
 *
 * @see plugin_api
 */
class FieldTypePluginManager extends DefaultPluginManager {

  /**
   * Define required keys for fields in bundle plugins.
   */
  const REQUIRED_KEYS = [
    'entity_reference' => [
      'storage_settings' => [
        'target_type',
      ],
      'field_settings' => [
        'handler',
        // phpcs:ignore
        'handler_settings' => [
          'target_bundles',
        ],
      ],
    ],
    'list_string' => [
      'storage_settings' => [
        'allowed_values|allowed_values_function',
      ],
    ],
  ];

  /**
   * Constructs a FieldTypePluginManager instance.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   */
  public function __construct(ModuleHandlerInterface $module_handler, CacheBackendInterface $cache_backend) {
    $this->factory = new ContainerFactory($this);
    $this->moduleHandler = $module_handler;
    $this->alterInfo('yaml_bundles_field_type_info');
    $this->setCacheBackend($cache_backend, 'yaml_bundles_field_type_plugins');
  }

  /**
   * {@inheritdoc}
   */
  protected function getDiscovery(): DiscoveryInterface {
    // @phpstan-ignore-next-line
    if (!isset($this->discovery)) {
      $yaml_discovery = new WeightYamlDiscovery('yaml_bundles.field_type', $this->moduleHandler->getModuleDirectories());
      $this->discovery = new ContainerDerivativeDiscoveryDecorator($yaml_discovery);
    }
    return $this->discovery;
  }

}
