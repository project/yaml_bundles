CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Custom plugin types
 * Drush commands


INTRODUCTION
------------

YAML Bundles is a pragmatic approach to streamlining content type management
through YAML based Drupal plugins. It allows developers to define fields and
content types from custom modules, making it a lot easier to add or update
fields and content types.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

   

CUSTOM PLUGIN TYPES
-------------
The module defines 3 important plugin types:

* Field types:
  By defining common field types and their form/display settings (like widgets
  and formatters), you can remove repetitive configuration from the bundle
  definitions.

* Entity types
  You can define common settings for entity types to remove even more
  repetitive configuration from the bundle definitions. We have also integrated
  support for a bunch of other contrib modules we often use, to save even more
  time.

* Entity bundles
  You can use the defined fields in entity bundles, complete with customizable
  form and display settings that can be overridden on a field-by-field basis.
  The default settings of the entity types can also be overridden if you need
  to.

The module ships with defaults for the most common field and entity types, but
you can easily override them. We have tests, which we also used to document all
the available plugin settings and options. You can check out the documentation
in the plugin definitions of the `yaml_bundles_test` module.


DRUSH COMMANDS
-------------

To use the power of YAML bundles, we’ve added a couple of helpful Drush
commands that will revolutionize your content type management.

### Create bundles from YAML definitions
```drush yaml-bundles:create-bundles```

This command uses the defined plugins to (re)create all the specified bundles,
fields, and settings. Whether you're starting from scratch or optimizing an
existing configuration, this command ensures the seamless generation of all
your content types with a single command.

### Create a bundle using drush
```drush yaml-bundles:create-bundle```

You don’t have to use the bundle plugins if you don’t want to. For those who
prefer a more hands-on approach, this command offers the flexibility to create
a new content type directly through Drush.

### Create a field using drush
```drush yaml-bundles:create-field```

This command simplifies the process of adding new fields to existing content
types using default plugin configurations. With the ability to customize form
and view displays, this command makes adding fields to different content types
a lot easier.
